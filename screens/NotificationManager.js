import PushNotification from 'react-native-push-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import { Platform } from 'react-native';

class NotificationManager {
  configure = (onRegister, onNotification, onOpenNotification) => {
    PushNotification.configure({
      onRegister(token) {
        onRegister(token);
        console.log('[NotificationManager] onRegister token:', token);
      },

      onNotification(notification) {
        console.log('[NotificationManager] onNotification:', notification);
        const noti = notification;

        if (Platform.OS === 'ios') {
          if (notification.data.openedInForeground) {
            noti.userInteraction = true;
          }
        } else {
          noti.userInteraction = true;
        }

        if (notification.userInteraction) {
          onOpenNotification(notification);
        } else {
          onNotification(notification);
        }

        if (Platform.OS === 'ios') {
          if (!notification.data.openedInForeground) {
            notification.finish('backgroundFetchResultNoData');
          }
        } else {
          notification.finish('backgroundFetchResultNoData');
        }
      },
    });
  };

  buildAndroidNotification = (id, title, message, data = {}, options = {}) => ({
    id,
    autoCancel: true,
    largeIcon: options.largeIcon || 'ic_launcher',
    smallIcon: options.smallIcon || 'ic_launcher',
    bigText: message || '',
    subText: title || '',
    vibrate: options.vibrate || false,
    vibration: options.vibration || 300,
    priority: options.priority || 'high',
    importance: options.importance || 'high',
    data,
  });

  buildIOSNotification = (id, title, message, data = {}, options = {}) => ({
    alertAction: options.alertAction || 'view',
    category: options.category || '',
    userInfo: {
      id,
      item: data,
    },
  });

  showNotification = (id, title, message, data = {}, options = {}) => {
    PushNotification.localNotification({
      // Android//
      ...this.buildAndroidNotification(id, title, message, data, options),
      // IOS//
      ...this.buildIOSNotification(id, title, message, data, options),
      // Android and IOS propoties //
      title: title || '',
      message: message || '',
      playSound: options.playSound || false,
      soundName: options.soundName || 'default',
      userInteraction: false,
    });
  };

  cancelAllLocalNotification = () => {
    if (Platform.OS === 'ios') {
      PushNotificationIOS.removeAllDeliveredNotifications();
    } else {
      PushNotificationIOS.cancelAllLocalNotifications();
    }
  };

  unregister = () => {
    return PushNotification.unregister;
  };
}

const notificationManager = new NotificationManager();
export default notificationManager;
