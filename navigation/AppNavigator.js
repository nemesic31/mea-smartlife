import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import RegisterStack from './RegisterNavigator';

const RootSwitch = createSwitchNavigator({
  Auth: RegisterStack,
  App: MainTabNavigator,
});

const AppContainer = createAppContainer(RootSwitch);
export default AppContainer;
