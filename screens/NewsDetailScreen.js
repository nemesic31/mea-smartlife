import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  StatusBar,
  ActivityIndicator,
  TouchableOpacity,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    flex: 1,
  },
});

export default class NewsScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: '',
    headerLeft: () => (
      <TouchableOpacity onPress={() => navigation.navigate('News')}>
        <View>
          <Icon name="ios-arrow-back" style={{ fontSize: 25, color: '#E07333', marginLeft: 10 }} />
        </View>
      </TouchableOpacity>
    ),
  });

  state = {
    news: {},
    loading: true,
  };

  componentDidMount() {
    const { navigation } = this.props;
    const id = JSON.stringify(navigation.getParam('id', '000')).slice(1, -1);
    if (id !== '000') {
      axios
        .get(`https://yena.entercorp.net/api/news/${id}`)
        .then((response) =>
          this.setState({
            news: response.data.data,
            loading: false,
          })
        )
        .catch((err) => console.log(err));
    }
  }

  render() {
    const { news, loading } = this.state;
    const old = news.content;

    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <ScrollView ontentInsetAdjustmentBehavior="automatic" style={{ height: '100%' }}>
          {loading === true ? (
            <ActivityIndicator size="large" color="#F26F21" />
          ) : (
            <View>
              <Image
                source={{ uri: news.image }}
                style={{
                  width: '100%',
                  height: 250,
                }}
              />
              <View style={{ width: '90%', alignSelf: 'center', marginTop: 15 }}>
                <Text style={{ fontSize: 15, color: '#c6c6c6', fontWeight: 'bold' }}>
                  {news.category}
                </Text>
                <Text style={{ fontSize: 26, fontWeight: 'bold', color: '#F26F21' }}>
                  {news.title}
                </Text>
                <Text style={{ fontSize: 15 }}>{old.replace(/[<p></p>]/g, '')}</Text>
              </View>
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}

NewsScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
