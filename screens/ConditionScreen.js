import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, TouchableOpacity, StatusBar, Platform } from 'react-native';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import I18n from '../src/i18n';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 2,
    flexDirection: 'column',
  },
  textBox: { width: '95%', marginTop: 15 },
  textHeader: {
    fontSize: 28,
    fontWeight: 'bold',
    marginBottom: 10,
    color: '#E07333',
  },
  textHeader2: {
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 10,
  },
  textHeader3: {
    fontSize: 18,
    marginBottom: 10,
  },
});

export default class ConditionScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: '',
    headerShown: Platform.OS === 'ios',
    headerLeft: () => (
      <TouchableOpacity onPress={() => navigation.navigate('First')}>
        <View>
          <Icon name="ios-arrow-back" style={{ fontSize: 25, color: '#E07333', marginLeft: 10 }} />
        </View>
      </TouchableOpacity>
    ),
  });

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <StatusBar
          barStyle={Platform === 'ios' ? 'light-content' : 'dark-content'}
          backgroundColor="#fff"
        />

        <View style={styles.textBox}>
          <Text style={styles.textHeader}>{I18n.t('Condition.conditionHeader')}</Text>
          <Text style={styles.textHeader2}>{I18n.t('Condition.conditionHeader2')}</Text>
          <Text style={styles.textHeader3}>{I18n.t('Condition.conditionDetail')}</Text>
        </View>
        <View style={{ marginBottom: 15 }}>
          <Button
            buttonStyle={{
              marginRight: 10,
              marginLeft: 10,
              marginBottom: 10,
              borderRadius: 5,
              backgroundColor: '#F26F21',
              width: 350,
            }}
            title={I18n.t('Condition.accept')}
            titleStyle={{ color: '#fff' }}
            type="solid"
            onPress={() => navigate('Register')}
          />
        </View>
      </View>
    );
  }
}

ConditionScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
