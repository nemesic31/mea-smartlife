import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  StatusBar,
  Platform,
  Text,
  View,
  StyleSheet,
  ScrollView,
} from 'react-native';
import { Card, ListItem } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import I18n from '../../src/i18n';

const styles = StyleSheet.create({
  background: {
    height: '100%',
    width: '100%',
  },
  textHeader: {
    fontSize: 15,
    marginLeft: 15,
  },
});

export default class DetailHistoryAlretScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('DetailHistory.detailHeader'),
    headerTintColor: '#fff',
    headerStyle: {
      backgroundColor: '#E07333',
    },
    headerLeft: () => {
      if (Platform.OS === 'ios') {
        return (
          <TouchableOpacity onPress={() => navigation.navigate('HistoryAlret')}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Icon name="ios-arrow-back" style={{ fontSize: 25, color: '#fff', marginLeft: 10 }} />
              <Text style={{ fontSize: 20, color: '#fff', marginLeft: 10 }}>
                {I18n.t('HeaderBar.back')}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
      return null;
    },
  });

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <StatusBar
          backgroundColor={Platform.OS === 'ios' ? 'white' : '#F26F21'}
          barStyle="dark-content"
        />
        <ScrollView ontentInsetAdjustmentBehavior="automatic">
          <Card style={{ borderRadius: 10 }}>
            <ListItem
              title="25/10/2562 4:30 PM"
              subtitle={I18n.t('DetailHistory.notifyPowerOutage')}
              bottomDivider
              titleStyle={{ fontSize: 13, marginBottom: 20, color: '#999' }}
              subtitleStyle={{ fontSize: 22, color: '#E07333' }}
            />
            <ListItem
              title={I18n.t('DetailHistory.notificationStatus')}
              bottomDivider
              titleStyle={{ fontSize: 17, color: '#E07333' }}
            />
            <Text style={styles.textHeader}>{I18n.t('DetailHistory.notifiedPosition')}</Text>
            <Text style={styles.textHeader}>{I18n.t('DetailHistory.addressAlret')}</Text>
          </Card>
          <Card style={{ borderRadius: 10 }}>
            <ListItem
              title="25/10/2562 4:31 PM"
              subtitle={I18n.t('DetailHistory.officer')}
              bottomDivider
              titleStyle={{ fontSize: 13, marginBottom: 20, color: '#999' }}
              subtitleStyle={{ fontSize: 22, color: '#E07333' }}
            />
            <ListItem
              title={I18n.t('DetailHistory.inProgress')}
              bottomDivider
              titleStyle={{ fontSize: 17, color: '#E07333' }}
            />
            <Text style={styles.textHeader}>{I18n.t('DetailHistory.officerStatus')}</Text>
          </Card>
        </ScrollView>
      </View>
    );
  }
}

DetailHistoryAlretScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
