import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Dimensions } from 'react-native';
import { BarChart } from 'react-native-chart-kit';
import { Table, Row, Rows } from 'react-native-table-component';
import I18n from '../../../src/i18n';

const chartConfig = {
  backgroundGradientFrom: '#fff',
  backgroundGradientTo: '#fff',
  color: (opacity = 1) => `rgb(224, 115, 51 , ${opacity})`,
  strokeWidth: 2, // optional, default 3
};

const screenWidth = Dimensions.get('window').width;

const barData = {
  labels: [
    I18n.t('CheckHistory.May'),
    I18n.t('CheckHistory.Jun'),
    I18n.t('CheckHistory.Jul'),
    I18n.t('CheckHistory.Aug'),
    I18n.t('CheckHistory.Sep'),
    I18n.t('CheckHistory.Oct'),
  ],
  datasets: [
    {
      data: [30, 20, 40, 50, 10, 5],
    },
  ],
};

export default class GraphKWH extends Component {
  render() {
    const { date, data } = this.props;
    const tableDataKWH = [
      [`24/10/${date}`, '30', '+10'],
      [`24/09/${date}`, '20', '-20'],
      [`24/08/${date}`, '40', '-10'],
      [`24/07/${date}`, '50', '+40'],
      [`24/06/${date}`, '10', '+5'],
      [`24/05/${date}`, '5', '-'],
    ];

    return (
      <View>
        <BarChart
          data={barData}
          width={screenWidth}
          height={220}
          yAxisLabel={''}
          chartConfig={chartConfig}
        />
        <Table
          borderStyle={{ borderWidth: 1, borderColor: '#DCDCDC', borderRadius: 1 }}
          style={{ marginTop: 20, alignSelf: 'center' }}>
          <Row
            data={data}
            textStyle={{ fontSize: 15, textAlign: 'center', color: '#E07333' }}
            widthArr={[150, 75, 150]}
          />
          <Rows
            data={tableDataKWH}
            textStyle={{ fontSize: 15, textAlign: 'center' }}
            widthArr={[150, 75, 150]}
            heightArr={[50, 50, 50, 50, 50, 50]}
          />
        </Table>
      </View>
    );
  }
}

GraphKWH.propTypes = {
  date: PropTypes.number,
  data: PropTypes.array.isRequired,
};
