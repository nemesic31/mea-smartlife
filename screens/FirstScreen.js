import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, StatusBar, Platform } from 'react-native';
import { Button } from 'react-native-elements';
import TouchID from 'react-native-touch-id';
import NavigationService from './NavigationService';
import fcmService from '../src/FCMservice';
import I18n from '../src/i18n';

const optionalConfigObject = {
  title: 'แสกนลายนิ้วมือ สำหรับเข้าสู่ระบบ', // Android
  imageErrorColor: 'red', // Android
  sensorDescription: 'แตะเซ็นเซอร์ลายนิ้วมือ', // Android
  cancelText: 'ยกเลิก', // Android
};

function authenticate() {
  return TouchID.authenticate('', optionalConfigObject)
    .then((success) => {
      // Alert.alert('Authenticated Successfully');
      NavigationService.navigate('Home');
    })
    .catch((error) => {
      NavigationService.navigate('Login');
    });
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#E07333',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  textHeader: {
    fontSize: 40,
    fontWeight: 'bold',
    color: '#fff',
    marginBottom: 44,
  },
  textHeader2: {
    fontWeight: 'bold',
    fontSize: 34,
    marginBottom: 50,
    color: '#fff',
  },
});

class FirstScreen extends Component {
  static navigationOptions = { headerShown: false };

  constructor(props) {
    super(props);
    this.state = {
      biometryType: null,
    };
    // setI18nConfig(); // set initial config
  }

  componentDidMount() {
    fcmService.register(this.onRegister, this.onNotification, this.onOpenNotification);
    TouchID.isSupported().then((biometryType) => {
      this.setState({ biometryType });
    });
  }

  onRegister(token) {
    console.log('[NotificationFCM] onRegister: ', token);
  }

  onNotification(notify) {
    console.log('[NotificationFCM] onNotification: ', notify);
    // For Android
    const channelObj = {
      channelId: 'SampleChannelID',
      channelName: 'SampleChannelName',
      channelDes: 'SampleChannelDes',
    };
    const channel = fcmService.buildChannel(channelObj);

    const buildNotify = {
      dataId: notify.notificationId,
      title: notify.title,
      content: notify.body,
      sound: 'default',
      channel,
      data: {},
      colorBgIcon: '#1A243B',
      largeIcon: 'ic_launcher',
      smallIcon: 'ic_launcher',
      vibrate: true,
    };

    const notification = fcmService.buildNotification(buildNotify);
    fcmService.displayNotification(notification);
  }

  onOpenNotification(notify) {
    console.log('[NotificationFCM] onOpenNotification: ', notify);
    alert(`Open Notification: ${notify.body}`);
  }

  pressHandler() {
    TouchID.isSupported()
      .then(authenticate)
      .catch((error) => {
        NavigationService.navigate('Login');
      });
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <StatusBar
          barStyle={Platform === 'ios' ? 'dark-content' : 'light-content'}
          backgroundColor="#E07333"
        />

        <Text style={styles.textHeader}>MEA</Text>

        <Text style={styles.textHeader2}>Smart Life</Text>

        <Button
          buttonStyle={{
            marginBottom: 14,
            borderRadius: 25,
            backgroundColor: '#ECECEC',
            width: 350,
          }}
          title={I18n.t('FirstScreen.signUp')}
          titleStyle={{ color: '#E07333' }}
          type="solid"
          onPress={() => navigate('Condition')}
        />

        <Button
          buttonStyle={{
            width: 400,
          }}
          title={I18n.t('FirstScreen.login')}
          titleStyle={{ color: '#fff' }}
          type="clear"
          onPress={this.pressHandler}
        />
      </View>
    );
  }
}

FirstScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const errors = {
  LAErrorAuthenticationFailed:
    'Authentication was not successful because the user failed to provide valid credentials.',
  LAErrorUserCancel:
    'Authentication was canceled by the user—for example, the user tapped Cancel in the dialog.',
  LAErrorUserFallback:
    'Authentication was canceled because the user tapped the fallback button (Enter Password).',
  LAErrorSystemCancel:
    'Authentication was canceled by system—for example, if another application came to foreground while the authentication dialog was up.',
  LAErrorPasscodeNotSet:
    'Authentication could not start because the passcode is not set on the device.',
  LAErrorTouchIDNotAvailable:
    'Authentication could not start because Touch ID is not available on the device',
  LAErrorTouchIDNotEnrolled:
    'Authentication could not start because Touch ID has no enrolled fingers.',
  RCTTouchIDUnknownError: 'Could not authenticate for an unknown reason.',
  RCTTouchIDNotSupported: 'Device does not support Touch ID.',
};

export default FirstScreen;
