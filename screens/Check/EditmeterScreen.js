import React, { Component } from 'react';
import { TouchableOpacity, Platform, Text, View, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import ToggleSwitch from 'toggle-switch-react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';
import { Hoshi } from 'react-native-textinput-effects';
import I18n from '../../src/i18n';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#ffffff',
    // height: '100%',
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'space-between',
    // alignItems: 'center',
  },
  textHeader: {
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 10,
    marginBottom: 20,
    marginTop: 8,
  },
  inputStyle: {
    marginBottom: 15,
  },
});

export default class EditmeterScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('editMeter.editMeterHeader'),
    headerRight: () => (
      <Button
        buttonStyle={{
          marginRight: 8,
        }}
        title={I18n.t('HeaderBar.complete')}
        titleStyle={{ color: '#E07333' }}
        type="clear"
        onPress={() => navigation.navigate('CheckHistory')}
      />
    ),
    headerLeft: () => {
      if (Platform.OS === 'ios') {
        return (
          <TouchableOpacity onPress={() => navigation.navigate('CheckHistory')}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Icon
                name="ios-arrow-back"
                style={{ fontSize: 25, color: '#E07333', marginLeft: 10 }}
              />
              <Text style={{ fontSize: 20, color: '#E07333', marginLeft: 10 }}>
                {I18n.t('HeaderBar.back')}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
      return null;
    },
  });

  constructor() {
    super();
    this.state = {
      toggle: false,
    };
  }

  check = (event) => {
    console.log(event);
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <View style={{ marginTop: 8 }}>
          <Text style={styles.textHeader}>{I18n.t('Namemeter.meterDataHeader')}</Text>

          <Hoshi
            style={{ marginBottom: 12 }}
            label={I18n.t('Fillmeter.inputMeter')}
            // this is used as active border color
            borderColor={'#F26F21'}
            // active border height
            borderHeight={3}
            inputPadding={16}
          />

          <Hoshi
            style={{ marginBottom: 12 }}
            label={I18n.t('Fillmeter.inputMeter2')}
            // this is used as active border color
            borderColor={'#F26F21'}
            // active border height
            borderHeight={3}
            inputPadding={16}
          />

          <Hoshi
            style={{ marginBottom: 12 }}
            label={I18n.t('Namemeter.ownerMeter')}
            // this is used as active border color
            borderColor={'#F26F21'}
            // active border height
            borderHeight={3}
            inputPadding={16}
          />

          <Hoshi
            style={{ marginBottom: 12 }}
            label={I18n.t('Namemeter.address')}
            // this is used as active border color
            borderColor={'#F26F21'}
            // active border height
            borderHeight={3}
            inputPadding={16}
          />

          <Hoshi
            style={{ marginBottom: 12 }}
            label={I18n.t('Namemeter.nameMeterInput')}
            // this is used as active border color
            borderColor={'#F26F21'}
            // active border height
            borderHeight={3}
            inputPadding={16}
          />
          <ToggleSwitch
            isOn={this.state.toggle}
            onColor="#E07333"
            offColor="gray"
            label={I18n.t('Namemeter.patientToggle')}
            labelStyle={{ marginLeft: 10, fontSize: 17, marginRight: 25 }}
            size="large"
            onToggle={(isOn) =>
              this.state.toggle ? this.setState({ toggle: false }) : this.setState({ toggle: true })
            }
          />
          <View style={{ backgroundColor: '#efeff4', marginTop: 10 }}>
            <Text
              style={{
                marginLeft: 20,
                marginTop: 10,
                marginBottom: 10,
                color: '#8a8a8f',
                fontWeight: '500',
                textAlign: 'left',
              }}>
              {I18n.t('Namemeter.patientDetail')}
            </Text>
          </View>
        </View>

        <View>
          <Button
            buttonStyle={{
              alignContent: 'center',
              marginBottom: 20,
              borderRadius: 10,

              width: 400,
            }}
            title={I18n.t('editMeter.deleteMeter')}
            titleStyle={{ color: '#E07333', textAlign: 'center' }}
            type="clear"
            onPress={() => navigate('Home')}
          />
        </View>
      </View>
    );
  }
}

EditmeterScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
