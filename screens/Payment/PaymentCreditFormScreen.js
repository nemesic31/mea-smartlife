import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  I18nManager,
  KeyboardAvoidingView,
  ScrollView,
  Platform,
} from 'react-native';
import { Button, Divider } from 'react-native-elements';
import ToggleSwitch from 'toggle-switch-react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Hoshi } from 'react-native-textinput-effects';
import I18n from '../../src/i18n';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
  },
  dividerStyle: {
    marginTop: 20,
    marginBottom: 20,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#000',
  },
  textHeader: {
    fontSize: 35,
    fontWeight: 'bold',
    marginBottom: 10,
    marginTop: 20,
    marginLeft: 20,
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 10,
    marginLeft: 20,
  },
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
});

export default class PaymentCreditFormScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('Payment.paymentHeader'),
    headerLeft: () => {
      if (Platform.OS === 'ios') {
        return (
          <TouchableOpacity onPress={() => navigation.navigate('SelectPayment')}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Icon
                name="ios-arrow-back"
                style={{ fontSize: 25, color: '#E07333', marginLeft: 10 }}
              />
              <Text style={{ fontSize: 20, color: '#E07333', marginLeft: 10 }}>
                {I18n.t('HeaderBar.back')}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
      return null;
    },
  });

  constructor() {
    super();
    this.state = {
      toggle: false,
    };
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.background}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          enabled={Platform.OS === 'ios'}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 60 : 0}>
          <ScrollView ontentInsetAdjustmentBehavior="automatic">
            <View>
              <Text
                style={{
                  fontSize: 18,
                  marginBottom: 16,
                  marginLeft: 20,
                  marginTop: 15,
                  fontWeight: 'bold',
                }}>
                {`Sky's Home`}
              </Text>
              <Text style={{ fontSize: 14, marginBottom: 5, marginLeft: 20, color: '#4d4d4d' }}>
                {I18n.t('Payment.accountShowingContract')} 213456789
              </Text>
              <Text style={{ fontSize: 14, marginBottom: 14, marginLeft: 20, color: '#999999' }}>
                {I18n.t('Payment.addressPay')}
              </Text>
              <Text style={{ fontSize: 22, marginBottom: 5, marginLeft: 15 }}>
                {' '}
                {I18n.t('SelectPayment.fee')}{' '}
              </Text>
              <Text
                style={{
                  fontSize: 22,
                  marginBottom: 5,
                  marginLeft: 20,
                  fontWeight: 'bold',
                  color: '#E07333',
                }}>
                6.00 {I18n.t('Payment.baht')}
              </Text>
              <Text style={{ fontSize: 22, marginBottom: 5, marginLeft: 15 }}>
                {' '}
                {I18n.t('PaymentcreditForm.totalPayment')}{' '}
              </Text>
              <Text
                style={{
                  fontSize: 34,
                  marginBottom: 5,
                  marginLeft: 20,
                  fontWeight: 'bold',
                  color: '#E07333',
                }}>
                204.42 {I18n.t('Payment.baht')}
              </Text>
            </View>
            <Divider style={styles.dividerStyle} />
            <View>
              <Text style={{ fontSize: 22, marginBottom: 4, marginLeft: 20, fontWeight: 'bold' }}>
                {I18n.t('PaymentcreditForm.inputEmailHeader')}
              </Text>
              <Hoshi
                style={{ marginBottom: 12 }}
                label={I18n.t('PaymentcreditForm.inputEmail')}
                // this is used as active border color
                borderColor={'#F26F21'}
                // active border height
                borderHeight={3}
                inputPadding={16}
              />
              <ToggleSwitch
                isOn={this.state.toggle}
                onColor="#E07333"
                offColor="gray"
                label={I18n.t('PaymentcreditForm.toggleEmail')}
                labelStyle={{ marginLeft: 25, fontSize: 17, marginRight: 20 }}
                size="large"
                onToggle={(isOn) =>
                  this.state.toggle
                    ? this.setState({ toggle: false })
                    : this.setState({ toggle: true })
                }
              />
            </View>
            <View style={{ marginTop: 40 }}>
              <Button
                title={I18n.t('Register.next')}
                titleStyle={{ color: '#fff' }}
                type="clear"
                buttonStyle={{
                  backgroundColor: '#E07333',
                  borderRadius: 10,
                  marginLeft: 15,
                  marginRight: 15,
                }}
                onPress={() => navigate('VerifyPayment')}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

PaymentCreditFormScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
