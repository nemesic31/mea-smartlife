import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Dimensions,
  PermissionsAndroid,
  Image,
  Text,
  Animated,
} from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';
import MapView, { Marker, ProviderPropType, Callout } from 'react-native-maps';
import { Button, Input } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import SlidingUpPanel from 'rn-sliding-up-panel';
import Icon from 'react-native-vector-icons/Entypo';
import I18n from '../../../src/i18n';
import NavigationService from '../../NavigationService';
import notificationManager from '../../NotificationManager';

const styles = StyleSheet.create({
  containerPanel: {
    // height: '100%',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  panel: {
    flex: 1,
    backgroundColor: 'white',
    position: 'relative',
  },
  panelHeader: {
    height: 120,
    backgroundColor: '#fbfbfd',
    padding: 24,
  },
  textTitleHeader: {
    fontSize: 20,
    color: '#E07333',
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

Geocoder.init('AIzaSyANMqeZOb82fO-iidbI0if4i9hM_eWvlY8');

class MapPublicScreen extends React.Component {
  static navigationOptions = { headerShown: false };

  constructor(props) {
    super(props);
    this.localNotify = {};
  }

  draggedValue = new Animated.Value(150);

  state = {
    mapRegion: null,
    latitude: null,
    longitude: null,
    markers: [],
    buttonHide: false,
    photo: null,
    address: null,
    showPanel: false,
    icon: 'chevron-small-up',
  };

  async componentDidMount() {
    this.getCurrentPosition();
    // this.requestLocationPermission();
    this.localNotify = notificationManager;
    this.localNotify.configure(this.onRegister, this.onNotification, this.onOpenNotification);
  }

  componentWillUnmount() {
    Geolocation.clearWatch(this.watchID);
  }

  onRegister(token) {
    console.log('[NotificationManager] Registered: ', token);
  }

  onNotification(Notify) {
    console.log('[NotificationManager] onNotification: ', Notify);
  }

  onOpenNotification(Notify) {
    console.log('[NotificationManager] onOpenNotification: ', Notify);
    NavigationService.navigate('HistoryAlret');
  }

  async requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Smart Life',
          message: 'Smart Life access to your location ',
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the location');
        this.getCurrentPosition();
      } else {
        console.log('location permission denied');
        alert('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  getCurrentPosition = () => {
    this.watchID = Geolocation.getCurrentPosition(
      (position) => {
        const region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        };
        Geocoder.from(position.coords.latitude, position.coords.longitude)
          .then((json) => {
            this.setState({ address: json.results[2].formatted_address });
          })
          .catch((error) => console.log(error));
        this.onRegionChange(region, region.latitude, region.longitude);
      },
      (error) => console.log(error),
      { enableHighAccuracy: true, timeout: 200000 }
    );
  };

  onRegionChange = (region, lastLat, lastLong) => {
    this.setState({
      mapRegion: region,
      // If there are no new values set the current ones
      latitude: lastLat || this.state.latitude,
      longitude: lastLong || this.state.longitude,
    });
  };

  handleChoosePhoto = () => {
    const options = {
      noData: true,
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.uri) {
        this.setState({ photo: response, buttonHide: true });
      }
    });
  };

  onPressSendNotification = () => {
    const options = {
      soundName: 'default',
      playSound: true,
      vibrate: true,
    };

    this.localNotify.showNotification(1, 'SmartLife', I18n.t('Map.mapNotifiy'), {}, options);

    this.props.navigation.navigate('CompleteProcessPublic');
  };

  onpressShowPanel = (event) => {
    if (!this.state.showPanel) {
      this.setState({ showPanel: true, icon: 'chevron-small-down' });

      this.panel.show();
    } else {
      this.setState({ showPanel: false, icon: 'chevron-small-up' });
      this.panel.hide();
    }
  };

  render() {
    const { navigate } = this.props.navigation;
    const { buttonHide, photo, latitude, longitude, mapRegion, address, icon } = this.state;

    return (
      <View style={styles.container}>
        <MapView
          provider={this.props.provider}
          style={styles.map}
          region={mapRegion}
          onPoiClick={this.onPoiClick}>
          {longitude && latitude && (
            <Marker
              pinColor="red"
              coordinate={{
                latitude,
                longitude,
              }}>
              <Callout style={{ width: 200 }}>
                <View>
                  <Text style={{ fontWeight: 'bold' }}>{address}</Text>
                </View>
                <View style={{ marginTop: 8 }}>
                  <Text>latitude: {latitude}</Text>
                  <Text>longitude:{longitude}</Text>
                </View>
              </Callout>
            </Marker>
          )}
        </MapView>

        <SlidingUpPanel
          ref={(c) => {
            this.panel = c;
          }}
          draggableRange={{ top: 580, bottom: 150 }}
          animatedValue={this.draggedValue}
          height={height + 120}
          friction={1.5}
          snappingPoints={[580]}
          showBackdrop={false}>
          <View style={styles.panel}>
            <Icon
              name={icon}
              style={{
                fontSize: 30,
                color: '#999',

                textAlign: 'center',
              }}
              onPress={this.onpressShowPanel}
            />
            <View style={styles.panelHeader}>
              <Animated.View>
                <Text style={styles.textTitleHeader}>{I18n.t('Map.mapHeader')}</Text>
                <Text style={{ fontSize: 14, color: '#999' }}>{address}</Text>
              </Animated.View>
            </View>
            <View style={styles.containerPanel}>
              <Input
                containerStyle={{ marginBottom: 10, marginLeft: 15 }}
                placeholderTextColor="#999"
                placeholder={I18n.t('MapPublic.mapPublicInput')}></Input>
              <Input
                containerStyle={{ marginBottom: 10, marginLeft: 15 }}
                placeholderTextColor="#999"
                placeholder={I18n.t('MapPublic.mapPublicInput2')}></Input>
              <Input
                containerStyle={{ marginBottom: 10, marginLeft: 15 }}
                placeholderTextColor="#999"
                placeholder={I18n.t('MapPublic.mapPublicInput3')}></Input>
              <View
                style={{
                  backgroundColor: '#fbfbfd',
                }}>
                <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                  {photo && (
                    <Image source={{ uri: photo.uri }} style={{ width: 200, height: 200 }} />
                  )}
                </View>
                {!buttonHide && (
                  <Button
                    iconRight={{ name: 'navigate-next', color: '#F26F21' }}
                    title={I18n.t('MapPublic.choosePhoto')}
                    titleStyle={{ color: '#E07333' }}
                    onPress={this.handleChoosePhoto}
                    type="outline"
                    buttonStyle={{
                      borderRadius: 5,
                      marginLeft: 10,
                      marginRight: 10,
                      borderColor: '#E07333',
                    }}
                  />
                )}
                <Button
                  title={I18n.t('Map.mapButton')}
                  titleStyle={{ color: '#fff' }}
                  type="clear"
                  buttonStyle={{
                    backgroundColor: '#E07333',
                    borderRadius: 10,
                    marginLeft: 10,
                    marginRight: 10,
                    marginTop: 5,
                  }}
                  onPress={this.onPressSendNotification}
                />
              </View>
            </View>
          </View>
        </SlidingUpPanel>
      </View>
    );
  }
}

MapPublicScreen.propTypes = {
  provider: ProviderPropType,
};

MapPublicScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default MapPublicScreen;
