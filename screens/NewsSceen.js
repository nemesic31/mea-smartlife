import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  StatusBar,
  ActivityIndicator,
  TouchableOpacity,
  Image,
  FlatList,
  RefreshControl,
} from 'react-native';
import Icons from 'react-native-vector-icons/MaterialIcons';
import I18n from '../src/i18n';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
  },
});

export default class NewsScreen extends Component {
  static navigationOptions = { headerShown: false };

  state = {
    news: [],
    isLoading: true,
  };

  componentDidMount() {
    this.setState({ news: [], isLoading: true });
    this.getData();
    console.log(this.state.isLoading);
    console.log('-------cd--------');
  }

  getData = () => {
    axios
      .get('https://yena.entercorp.net/api/news')
      .then((response) => {
        const { data } = response.data;
        this.setState({ isLoading: false, news: data.articles });
      })
      .catch((error) => {
        this.setState({ isLoading: false, error: 'Something just went wrong' });
      });
    console.log(this.state.isLoading);
    console.log('-------gd--------');
  };

  onRefresh = () => {
    this.setState({ isLoading: true });
    this.getData();
    console.log(this.state.isLoading);
    console.log('-------or--------');
  };

  renderRow = ({ item }) => {
    const { navigate } = this.props.navigation;
    return (
      <View>
        <TouchableOpacity key={item.id} onPress={() => navigate('NewsDetail', { id: item.id })}>
          <View
            style={{
              backgroundColor: '#000',
              background: 'linear-gradient(180deg, #000000 0%, rgba(0, 0, 0, 0) 100%)',
              borderRadius: 10,
              height: 400,
              marginBottom: 10,
              width: '94%',
              alignSelf: 'center',
            }}>
            <Image
              source={{ uri: item.image }}
              style={{
                width: '100%',
                height: 400,
                position: 'absolute',
                opacity: 0.4,
                justifyContent: 'center',
                borderRadius: 10,
                flex: 1,
              }}
            />
            <View style={{ padding: 10 }}>
              <Text
                style={{
                  marginBottom: 5,
                  fontSize: 18,
                  color: '#dfdfdf',
                  fontWeight: 'bold',
                }}>
                {item.category}
              </Text>
              <Text
                style={{
                  marginBottom: 5,
                  fontSize: 24,
                  fontWeight: 'bold',
                  color: '#F26F21',
                }}>
                {item.title}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const { navigate } = this.props.navigation;
    const { news } = this.state;
    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />

        <View
          style={{
            alignItems: 'flex-end',
          }}>
          <Icons
            style={{ marginRight: 8, marginTop: 35 }}
            name="notifications"
            size={30}
            color="#F26F21"
            onPress={() => navigate('Notification')}
          />
        </View>

        <Text
          style={{
            fontSize: 34,
            fontWeight: 'bold',
            marginLeft: 16,
            marginBottom: 8,
            color: '#F26F21',
          }}>
          {I18n.t('News.newsHeader')}
        </Text>
        {this.state.isLoading ? (
          <ActivityIndicator size="large" color="#F26F21" />
        ) : (
          <ScrollView
            ontentInsetAdjustmentBehavior="automatic"
            style={{ height: '100%' }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isLoading}
                onRefresh={this.onRefresh}
                tintColor="transparent"
                colors={['transparent']}
              />
            }>
            <FlatList
              data={news}
              renderItem={this.renderRow}
              keyExtractor={(i, k) => k.toString()}
            />
          </ScrollView>
        )}
      </View>
    );
  }
}

NewsScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
