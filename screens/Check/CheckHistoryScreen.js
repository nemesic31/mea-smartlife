import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Platform,
  ScrollView,
  StatusBar,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import { Button, ButtonGroup } from 'react-native-elements';
import axios from 'axios';
import i18n from 'i18n-js';
import Icon from 'react-native-vector-icons/Ionicons';
import I18n from '../../src/i18n';
import GraphKWH from './components/GraphKWH';
import GraphBath from './components/GraphBath';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#ffffff',
    height: '100%',
    flex: 1,
  },
  textHeader: {
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 10,
    marginBottom: 20,
    marginTop: 15,
  },
});

export default class CheckHistoryScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('CheckHistory.detailHome'),
    headerRight: () => (
      <Button
        buttonStyle={{
          marginRight: 16,
        }}
        title={I18n.t('HeaderBar.edit')}
        titleStyle={{ fontSize: 20, color: '#E07333' }}
        type="clear"
        onPress={() => navigation.navigate('Editmeter')}
      />
    ),
    headerLeft: () => {
      if (Platform.OS === 'ios') {
        return (
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Icon name="ios-arrow-back" style={{ fontSize: 25, color: '#fff', marginLeft: 10 }} />
              <Text style={{ fontSize: 20, color: '#fff', marginLeft: 10 }}>
                {I18n.t('HeaderBar.back')}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
      return null;
    },
  });

  constructor() {
    super();
    this.state = {
      date: '',
      selectedIndex: 0,
      version: '0',
      refresh: false,
      tableHeadBath: [
        I18n.t('CheckHistory.dateNote'),
        I18n.t('CheckHistory.electricBillBaht'),
        I18n.t('CheckHistory.Difference'),
      ],
      tableHeadKWH: [
        I18n.t('CheckHistory.dateNote'),
        I18n.t('CheckHistory.electricBillKWh'),
        I18n.t('CheckHistory.Difference'),
      ],
    };
  }

  componentDidMount() {
    const configs = i18n.locale;
    if (configs === 'th-TH') {
      const year = new Date().getFullYear() + 543; // Current Year
      this.setState({
        // Setting the value of the date time
        date: year,
      });
    } else {
      const year = new Date().getFullYear();
      this.setState({
        // Setting the value of the date time
        date: year,
      });
    }
    this.getVersion();
  }

  getVersion = () => {
    axios
      .get('https://yena.entercorp.net/api/admin/app-version')
      .then((response) => {
        const { version } = response.data.data;
        this.setState({ version, refresh: false });
      })
      .catch((error) => {
        this.setState({ refresh: false });
        console.log(error);
      });
  };

  onRefresh = () => {
    this.setState({
      refresh: true,
    });
    this.getVersion();
  };

  updateIndex = (selectedIndex) => {
    this.setState({ selectedIndex });
  };

  render() {
    const buttons = [I18n.t('CheckHistory.moneyUnit'), I18n.t('CheckHistory.powerUnit')];
    const { selectedIndex, version, refresh } = this.state;
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <ScrollView
          contentContainerStyle={styles.scrollView}
          refreshControl={<RefreshControl refreshing={refresh} onRefresh={this.onRefresh} />}>
          {version === '0' && (
            <ActivityIndicator size="large" color="#F26F21" style={{ marginTop: 10 }} />
          )}
          {version === '2' && (
            <Text style={{ textAlign: 'center', fontSize: 20, marginTop: 25 }}>
              This is version 2
            </Text>
          )}
          {version === '1' && (
            <View>
              <Text style={styles.textHeader}>
                {I18n.t('CheckHistory.electricityUsageHistory')}
              </Text>
              <ButtonGroup
                onPress={this.updateIndex}
                selectedIndex={selectedIndex}
                buttons={buttons}
                containerStyle={{ marginBottom: 40 }}
                selectedButtonStyle={{ backgroundColor: '#E07333' }}
              />
              <View style={{ justifyContent: 'center', width: '100%' }}>
                <ScrollView ontentInsetAdjustmentBehavior="automatic" style={{ height: '100%' }}>
                  <View style={{ marginBottom: '50%' }}>
                    {this.state.selectedIndex === 0 ? (
                      <GraphBath data={this.state.tableHeadBath} date={this.state.date} />
                    ) : (
                      <GraphKWH data={this.state.tableHeadKWH} date={this.state.date} />
                    )}
                  </View>
                </ScrollView>
              </View>
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}

CheckHistoryScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
