import React from 'react';
import PropTypes from 'prop-types';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import { View, Text } from 'react-native';
import I18n from '../src/i18n';

import HomeScreen from '../screens/HomeScreen';
import NewsScreen from '../screens/NewsSceen';
import AlretScreen from '../screens/AlretScreen';
import SettingScreen from '../screens/SettingScreen';
import NotificationScreen from '../screens/NotificationScreen';

import NewsDetailScreen from '../screens/NewsDetailScreen';

// Home Stack
import CheckHistoryScreen from '../screens/Check/CheckHistoryScreen';
import EditmeterScreen from '../screens/Check/EditmeterScreen';
import SelectPaymentScreen from '../screens/Payment/SelectPaymentScreen';
import QRcodemeterScreen from '../screens/Payment/QRcodemeterScreen';
import PaymentCreditFormScreen from '../screens/Payment/PaymentCreditFormScreen';
import VerifyPaymentScreen from '../screens/Payment/VerifyPaymentScreen';
import BillPaymentScreen from '../screens/Payment/BillPaymentScreen';
import ScanQRcodeScreen from '../screens/Add-meter/ScanQRcodeScreen';
import FillmeterScreen from '../screens/Add-meter/FillmeterScreen';
import NamemeterScreen from '../screens/Add-meter/NamemeterScreen';

// Alert Stack
import HistoryAlertScreen from '../screens/HistoryInform/HistoryAlretScreen';
import DetailHistoryAlretScreen from '../screens/HistoryInform/DetailHistoryAlretScreen';
import SelectAlretScreen from '../screens/InformHome/SelectAlretScreen';
import MapScreen from '../screens/InformHome/MapScreen';
import CompleteProcessScreen from '../screens/InformHome/CompleteProcessScreen';
import SelectAlretPublicScreen from '../screens/InformPublic/InformHome/SelectAlretPublicScreen';
import MapPublicScreen from '../screens/InformPublic/InformHome/MapPublicScreen';
import CompleteProcessPublicScreen from '../screens/InformPublic/InformHome/CompleteProcessPublicScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
  CheckHistory: CheckHistoryScreen,
  Editmeter: EditmeterScreen,
  SelectPayment: SelectPaymentScreen,
  QRcodemeter: QRcodemeterScreen,
  PaymentCreditForm: PaymentCreditFormScreen,
  VerifyPayment: VerifyPaymentScreen,
  BillPayment: BillPaymentScreen,
  ScanQRcode: ScanQRcodeScreen,
  Fillmeter: FillmeterScreen,
  Namemeter: NamemeterScreen,
  Notification: NotificationScreen,
});

HomeStack.navigationOptions = ({ navigation }) => {
  if (navigation.state.index >= 1) {
    return {
      tabBarVisible: false,
    };
  }
  return {
    tabBarVisible: true,
  };
};

const AlretStack = createStackNavigator({
  Alret: AlretScreen,
  HistoryAlret: HistoryAlertScreen,
  DetailHistoryAlret: DetailHistoryAlretScreen,
  SelectAlret: SelectAlretScreen,
  Map: MapScreen,
  CompleteProcess: CompleteProcessScreen,
  SelectAlretPublic: SelectAlretPublicScreen,
  MapPublic: MapPublicScreen,
  CompleteProcessPublic: CompleteProcessPublicScreen,

  SelectPayment: SelectPaymentScreen,
});

AlretStack.navigationOptions = ({ navigation }) => {
  if (navigation.state.index >= 1) {
    return {
      tabBarVisible: false,
    };
  }
  return {
    tabBarVisible: true,
  };
};

const NewsStack = createStackNavigator({
  News: NewsScreen,
  SelectPayment: SelectPaymentScreen,
  NewsDetail: NewsDetailScreen,
});

NewsStack.navigationOptions = ({ navigation }) => {
  if (navigation.state.index >= 1) {
    return {
      tabBarVisible: false,
    };
  }
  return {
    tabBarVisible: true,
  };
};

const SettingStack = createStackNavigator({
  Setting: SettingScreen,
  SelectPayment: SelectPaymentScreen,
});

SettingStack.navigationOptions = ({ navigation }) => {
  if (navigation.state.index >= 1) {
    return {
      tabBarVisible: false,
    };
  }
  return {
    tabBarVisible: true,
  };
};

const HomeTabBarIcon = ({ tintColor }) => (
  <View style={{ alignItems: 'center' }}>
    <Icons name="home-outline" size={30} color={tintColor} />
    <Text style={{ color: tintColor }}>{I18n.t('MainTab.homeTab')}</Text>
  </View>
);

HomeTabBarIcon.propTypes = {
  tintColor: PropTypes.string.isRequired,
};

const AlretTabBarIcon = ({ tintColor }) => (
  <View style={{ alignItems: 'center' }}>
    <Icons name="alarm-light-outline" size={30} color={tintColor} />
    <Text style={{ color: tintColor }}>{I18n.t('MainTab.alretTab')}</Text>
  </View>
);

AlretTabBarIcon.propTypes = {
  tintColor: PropTypes.string.isRequired,
};

const NewsTabBarIcon = ({ tintColor }) => (
  <View style={{ alignItems: 'center' }}>
    <Icons name="bullhorn-outline" size={30} color={tintColor} />
    <Text style={{ color: tintColor }}>{I18n.t('MainTab.newsTab')}</Text>
  </View>
);

NewsTabBarIcon.propTypes = {
  tintColor: PropTypes.string.isRequired,
};

const SettingTabBarIcon = ({ tintColor }) => (
  <View style={{ alignItems: 'center' }}>
    <Icons name="dots-horizontal-circle-outline" size={30} color={tintColor} />
    <Text style={{ color: tintColor }}>{I18n.t('MainTab.settingTab')}</Text>
  </View>
);

SettingTabBarIcon.propTypes = {
  tintColor: PropTypes.string.isRequired,
};

const tabNavigator = createBottomTabNavigator(
  {
    Home: { screen: HomeStack, navigationOptions: { tabBarIcon: HomeTabBarIcon } },
    Alret: { screen: AlretStack, navigationOptions: { tabBarIcon: AlretTabBarIcon } },
    News: { screen: NewsStack, navigationOptions: { tabBarIcon: NewsTabBarIcon } },
    Setting: { screen: SettingStack, navigationOptions: { tabBarIcon: SettingTabBarIcon } },
  },
  {
    tabBarOptions: {
      activeTintColor: '#F26F21',
      inactiveTintColor: '#A3A3A3',
      showLabel: false,
      labelStyle: {
        fontSize: 14,
      },
      style: {
        height: 60,
      },
      tabStyle: {
        backgroundColor: '#fff',
      },
    },
  }
);

tabNavigator.propTypes = {
  navigation: PropTypes.object.isRequired,
  focused: PropTypes.bool.isRequired,
  horizontal: PropTypes.bool.isRequired,
  tintColor: PropTypes.string.isRequired,
};

export default tabNavigator;
