import React, { Component } from 'react';
import AppContainer from './navigation/AppNavigator';
import NavigationService from './screens/NavigationService';

export default class App extends Component {
  render() {
    return (
      <AppContainer
        ref={(navigatorRef) => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
