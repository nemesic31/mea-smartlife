import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Dimensions, Animated, Text } from 'react-native';
import { Button } from 'react-native-elements';
import MapView, { Marker, ProviderPropType, Callout } from 'react-native-maps';
import Geocoder from 'react-native-geocoding';
import SlidingUpPanel from 'rn-sliding-up-panel';
import Icon from 'react-native-vector-icons/Entypo';
import NavigationService from '../NavigationService';
import notificationManager from '../NotificationManager';
import I18n from '../../src/i18n';
import Map from './components/Map';

const styles = StyleSheet.create({
  containerPanel: {
    // height: '100%',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  panel: {
    flex: 1,
    backgroundColor: 'white',
    position: 'relative',
  },
  panelHeader: {
    height: 120,
    backgroundColor: '#fbfbfd',
    padding: 24,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
  background: {
    height: '100%',
    backgroundColor: '#fff',
  },
});

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let id = 0;

class MapScreen extends Component {
  static navigationOptions = { headerShown: false };

  draggedValue = new Animated.Value(150);

  constructor(props) {
    super(props);
    this.localNotify = {};

    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      address: null,
      markers: [],
      showPanel: false,
      icon: 'chevron-small-up',
    };
  }

  onMapPress(e) {
    this.setState({
      markers: [
        ...this.state.markers,
        {
          coordinate: e.nativeEvent.coordinate,
          key: (id += 1),
          color: 'red',
        },
      ],
    });
  }

  componentDidMount = () => {
    Geocoder.from(LATITUDE, LONGITUDE)
      .then((json) => {
        this.setState({ address: json.results[2].formatted_address });
      })
      .catch((error) => console.warn(error));

    this.localNotify = notificationManager;
    this.localNotify.configure(this.onRegister, this.onNotification, this.onOpenNotification);
  };

  onRegister(token) {
    console.log('[NotificationManager] Registered: ', token);
  }

  onNotification(Notify) {
    console.log('[NotificationManager] onNotification: ', Notify);
  }

  onOpenNotification(Notify) {
    console.log('[NotificationManager] onOpenNotification: ', Notify);
    NavigationService.navigate('HistoryAlret');
  }

  onPressSendNotification = () => {
    const options = {
      soundName: 'default',
      playSound: true,
      vibrate: true,
    };

    this.localNotify.showNotification(1, 'SmartLife', I18n.t('Map.mapNotifiy'), {}, options);

    this.props.navigation.navigate('CompleteProcess');
  };

  onpressShowPanel = (event) => {
    if (!this.state.showPanel) {
      this.setState({ showPanel: true, icon: 'chevron-small-down' });

      this.panel.show();
    } else {
      this.setState({ showPanel: false, icon: 'chevron-small-up' });
      this.panel.hide();
    }
  };

  render() {
    const { address, icon, region } = this.state;

    return (
      <View style={styles.background}>
        <View
          style={{
            width: '100%',
            height: '100%',
            justifyContent: 'flex-end',
          }}>
          <Map region={region} address={address} />
          <SlidingUpPanel
            ref={(c) => {
              this.panel = c;
            }}
            draggableRange={{ top: 250, bottom: 150 }}
            animatedValue={this.draggedValue}
            height={height + 120}
            friction={1.5}
            snappingPoints={[250]}
            showBackdrop={false}>
            <View style={styles.panel}>
              <Icon
                name={icon}
                style={{
                  fontSize: 30,
                  color: '#999',

                  textAlign: 'center',
                }}
                onPress={this.onpressShowPanel}
              />

              <View style={styles.panelHeader}>
                <Animated.View>
                  <Text style={{ color: '#E07333' }}>{I18n.t('Map.mapHeader')}</Text>
                  <Text style={{ fontSize: 14, color: '#999' }}>{address}</Text>
                </Animated.View>
              </View>
              <View style={styles.containerPanel}>
                <Button
                  title={I18n.t('Map.mapButton')}
                  type="solid"
                  buttonStyle={{
                    borderRadius: 10,
                    marginLeft: 10,
                    marginRight: 10,
                    marginBottom: 25,
                    backgroundColor: '#E07333',
                  }}
                  onPress={this.onPressSendNotification}
                />
              </View>
            </View>
          </SlidingUpPanel>
        </View>
      </View>
    );
  }
}

MapScreen.propTypes = {
  provider: ProviderPropType,
};

MapScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default MapScreen;
