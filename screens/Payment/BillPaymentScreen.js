import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, Platform, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import Icons from 'react-native-vector-icons/SimpleLineIcons';
import { Table, Rows } from 'react-native-table-component';
import Icon from 'react-native-vector-icons/Ionicons';
import I18n from '../../src/i18n';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',

    width: '100%',
  },
  textHeader: {
    fontSize: 35,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 10,
    color: '#E07333',
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 10,
    marginLeft: 20,
  },
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  container: {
    flex: 1,
    padding: 16,
    paddingTop: 30,
    backgroundColor: '#fff',
  },
  head: {
    height: 40,
    backgroundColor: '#f1f8ff',
  },
  text: {
    margin: 6,
    fontSize: 15,
  },
});

export default class BillPaymentScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('BillPayment.processPayment'),
    headerLeft: () => {
      if (Platform.OS === 'ios') {
        return (
          <TouchableOpacity onPress={() => navigation.navigate('VerifyPayment')}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Icon
                name="ios-arrow-back"
                style={{ fontSize: 25, color: '#E07333', marginLeft: 10 }}
              />
              <Text style={{ fontSize: 20, color: '#E07333', marginLeft: 10 }}>
                {I18n.t('HeaderBar.back')}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
      return null;
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      tableData: [
        [I18n.t('BillPayment.electricityBillNumber'), '576868924'],
        [I18n.t('Payment.accountShowingContract'), '213465879'],
        [I18n.t('BillPayment.customerName'), 'ท้องฟ้า แจ่มใส'],
        [I18n.t('BillPayment.electricBill'), I18n.t('BillPayment.valueEB')],
        [I18n.t('SelectPayment.fee'), I18n.t('BillPayment.valueFee')],
        [I18n.t('BillPayment.paymentDate'), '10/01/2562 15:30:25'],
        [I18n.t('PaymentcreditForm.totalPayment'), I18n.t('BillPayment.valueTotal')],
      ],
    };
  }

  render() {
    const { state } = this;
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={{ alignItems: 'center' }}>
          <Icons name="check" style={{ fontSize: 100, marginTop: 10, color: '#4CD964' }} />
          <Text style={styles.textHeader}>{I18n.t('Complete.statusComplete')}</Text>
        </View>
        <Table
          borderStyle={{ borderWidth: 2, borderColor: '#fff' }}
          style={{ marginLeft: 40, marginRight: 0 }}>
          <Rows data={state.tableData} textStyle={styles.text} />
        </Table>
        <Button
          title={I18n.t('BillPayment.backToMyHome')}
          titleStyle={{ color: '#fff' }}
          type="clear"
          buttonStyle={{
            backgroundColor: '#E07333',
            borderRadius: 10,
            marginLeft: 15,
            marginRight: 15,
            marginTop: 5,
            marginBottom: 20,
          }}
          onPress={() => navigate('Home')}
        />
      </View>
    );
  }
}

BillPaymentScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
