import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Platform,
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  StatusBar,
  ScrollView,
} from 'react-native';
import { ListItem } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import I18n from '../../../src/i18n';

const styles = StyleSheet.create({
  background: {
    height: '100%',
  },
  textHeader: {
    fontSize: 25,
    fontWeight: 'bold',
    marginBottom: 15,
    marginTop: 20,
    marginLeft: 10,
    color: '#F26F21',
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 40,
    marginLeft: 15,
    color: '#7D7E82',
  },
  textHeader3: {
    fontSize: 15,
    marginLeft: 15,
    marginBottom: 10,
    marginTop: 20,
    color: '#7D7E82',
  },
});

export default class SelectAlretPublicScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('InformHome.informHeader'),
    headerLeft: () => {
      if (Platform.OS === 'ios') {
        return (
          <TouchableOpacity onPress={() => navigation.navigate('Alret')}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Icon
                name="ios-arrow-back"
                style={{ fontSize: 25, color: '#F26F21', marginLeft: 10 }}
              />
              <Text style={{ fontSize: 20, color: '#F26F21', marginLeft: 10 }}>
                {I18n.t('HeaderBar.back')}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
      return null;
    },
  });

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <ScrollView ontentInsetAdjustmentBehavior="automatic">
          <Text style={styles.textHeader}>{I18n.t('SelectAlretPublic.informPubilcHeader')}</Text>
          <Text style={styles.textHeader2}>{I18n.t('InformHome.addressInform')}</Text>
          <Text style={styles.textHeader3}>{I18n.t('InformHome.selectHeader')}</Text>
          <ListItem
            title={I18n.t('SelectAlretPublic.listitemPubilcSelectHeader')}
            bottomDivider
            titleStyle={{ color: '#F26F21' }}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            onPress={() => navigate('MapPublic')}
          />
          <Text style={styles.textHeader3}>{I18n.t('InformHome.selectHeader2')}</Text>
          <ListItem
            title={I18n.t('SelectAlretPublic.listitemPubilcSelectHeader2')}
            bottomDivider
            titleStyle={{ color: '#F26F21' }}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            onPress={() => navigate('MapPublic')}
          />
          <ListItem
            title={I18n.t('SelectAlretPublic.listitem2PubilcSelectHeader2')}
            bottomDivider
            titleStyle={{ color: '#F26F21' }}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            onPress={() => navigate('MapPublic')}
          />

          <Text style={styles.textHeader3}>{I18n.t('InformHome.selectHeader3')}</Text>
          <ListItem
            title={I18n.t('SelectAlretPublic.listitemPubilcSelectHeader3')}
            titleStyle={{ color: '#F26F21' }}
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            onPress={() => navigate('MapPublic')}
          />
          <ListItem
            title={I18n.t('SelectAlretPublic.listitem2PubilcSelectHeader3')}
            titleStyle={{ color: '#F26F21' }}
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            onPress={() => navigate('MapPublic')}
          />
          <View style={{ margin: 20 }}></View>
        </ScrollView>
      </View>
    );
  }
}

SelectAlretPublicScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
