import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  StatusBar,
  Platform,
  KeyboardAvoidingView,
} from 'react-native';
import { Button } from 'react-native-elements';
import { Grid, Col } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import { Hoshi } from 'react-native-textinput-effects';
import I18n from '../src/i18n';

const styles = StyleSheet.create({
  gridPad: { paddingTop: 30, paddingBottom: 90 },
  txtMargin: { margin: 5, backgroundColor: '#F26F21' },
  background: {
    backgroundColor: '#fff',
    height: '100%',
    alignItems: 'center',
    width: '100%',
    flex: 1,
  },
  textBox: { alignItems: 'center', marginTop: 15, backgroundColor: '#F26F21' },
  textHeader: {
    fontSize: 34,
    fontWeight: 'bold',
    marginBottom: 8,
    marginTop: 20,
    color: '#F26F21',
  },
  textHeader2: {
    fontSize: 18,
    marginBottom: 10,
    color: '#8c8c8c',
  },
});

export default class VerifyScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: '',
    headerShown: Platform.OS === 'ios',
    headerLeft: () => (
      <TouchableOpacity onPress={() => navigation.navigate('Register')}>
        <View>
          <Icon name="ios-arrow-back" style={{ fontSize: 25, color: '#E07333', marginLeft: 10 }} />
        </View>
      </TouchableOpacity>
    ),
  });

  constructor(props) {
    super(props);
    this.state = { otp: [] };
    this.myRef = [];
  }

  renderInputs() {
    const inputs = Array(6).fill(0);
    const txt = inputs.map((i, j) => (
      <Col key={j} style={styles.txtMargin}>
        <Hoshi
          keyboardType="numeric"
          autoFocus={j === 0 && true}
          maxLength={1}
          secureTextEntry={true}
          onChangeText={(value) => this.focusNext(j, value)}
          onKeyPress={(e) => this.focusPrevious(e.nativeEvent.key, j)}
          ref={(ref) => {
            this.myRef[j] = ref;
          }}
          // this is used as active border color
          borderColor={'#F26F21'}
          // active border height
          borderHeight={3}
          inputPadding={16}
        />
      </Col>
    ));
    return txt;
  }

  focusPrevious(key, index) {
    if (key === 'Backspace' && index !== 0) this.myRef[index - 1].focus();
  }

  focusNext(index, value) {
    if (index < this.myRef.length - 1 && value) {
      this.myRef[index + 1].focus();
    }
    if (index === this.myRef.length - 1) {
      this.myRef[index].blur();
    }
    const { otp } = this.state;
    otp[index] = value;
    this.setState({ otp });
    if (otp.length === 6) {
      this.props.navigation.navigate('Home');
    }
  }

  render() {
    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          enabled={Platform.OS === 'ios'}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 60 : 0}>
          <View style={{ marginTop: 15 }}>
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Text style={styles.textHeader}>{I18n.t('Verify.verifyHeader')}</Text>
            </View>
            <View style={{ alignItems: 'center', justifyContent: 'center', width: '98%' }}>
              <Text style={styles.textHeader2}>{I18n.t('Verify.verifyDetail')}</Text>
            </View>
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Grid style={styles.gridPad}>{this.renderInputs()}</Grid>
            </View>
            <View style={{ marginBottom: 10, alignItems: 'center', justifyContent: 'flex-end' }}>
              <Button
                title={I18n.t('Verify.resend')}
                titleStyle={{ color: '#8C8C8C' }}
                type="clear"
              />
            </View>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

VerifyScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
