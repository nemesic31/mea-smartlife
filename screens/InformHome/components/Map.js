import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet } from 'react-native';
import MapView, { Marker, ProviderPropType, Callout } from 'react-native-maps';

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default class Map extends Component {
  render() {
    const { region, address, provider } = this.props;
    console.log(address);

    return (
      <View style={{ width: '100%', height: '100%' }}>
        <MapView provider={provider} style={styles.map} initialRegion={region}>
          <Marker coordinate={region}>
            <Callout style={{ width: 200 }}>
              <View>
                <Text style={{ fontWeight: 'bold' }}>{address}</Text>
              </View>
              <View style={{ marginTop: 8 }}>
                <Text>latitude: {region.latitude}</Text>
                <Text>longitude:{region.longitude}</Text>
              </View>
            </Callout>
          </Marker>
        </MapView>
      </View>
    );
  }
}

Map.propTypes = {
  provider: ProviderPropType,
};

Map.propTypes = {
  region: PropTypes.object,
  address: PropTypes.string,
};
