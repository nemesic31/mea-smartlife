import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, StatusBar, StyleSheet, ScrollView, Platform } from 'react-native';
import { Card, Button } from 'react-native-elements';
import Icons from 'react-native-vector-icons/MaterialIcons';
import I18n from '../src/i18n';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
  },
});

export default class HomeScreen extends Component {
  static navigationOptions = { headerShown: false };

  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.getMeter();
    this.listener = navigation.addListener('didFocus', this.getMeter);
  }

  componentWillUnmount() {
    this.listener.remove();
  }

  getMeter = () => {
    const { navigation } = this.props;
    try {
      const { data } = navigation.state.params;
      this.setState({ data });
    } catch (err) {
      console.log(err);
    }
  };

  showCard = () => {
    const { navigate } = this.props.navigation;
    const { data } = this.state;
    if (Object.keys(data).length !== 0) {
      return (
        <Card containerStyle={{ borderRadius: 10 }}>
          <View style={{ flexDirection: 'row', width: '100%' }}>
            <Text style={{ marginBottom: 5, fontSize: 25, textAlign: 'left', width: '50%' }}>
              {data.meter_name}
            </Text>
            <Icons
              name="navigate-next"
              style={{ fontSize: 25, textAlign: 'right', width: '50%' }}
              onPress={() => navigate('CheckHistory')}
            />
          </View>
          <Text style={{ marginBottom: 5, fontSize: 25 }}>{I18n.t('Home.electricityBill')}</Text>
          <Text style={{ marginBottom: 5, fontSize: 30, fontWeight: 'bold', color: '#F26F21' }}>
            198.42
          </Text>
          <Text style={{ marginBottom: 5, fontSize: 20 }}>{I18n.t('Home.due')} 10/11/2562</Text>
          <Button
            buttonStyle={{
              marginLeft: 0,
              marginRight: 0,
              marginBottom: 0,
              borderRadius: 10,
              backgroundColor: '#F26F21',
            }}
            title={I18n.t('Home.pay')}
            titleStyle={{ color: '#fff' }}
            type="clear"
            onPress={() => navigate('SelectPayment')}
          />
        </Card>
      );
    }
  };

  render() {
    const { navigate } = this.props.navigation;
    const { data } = this.state;

    return (
      <View style={styles.background}>
        <StatusBar
          backgroundColor={Platform.OS === 'ios' ? 'white' : '#F26F21'}
          barStyle="dark-content"
        />
        <ScrollView ontentInsetAdjustmentBehavior="automatic" style={{ height: '100%' }}>
          <View style={{ backgroundColor: '#F26F21', height: '22%' }}>
            <View
              style={{
                alignItems: 'flex-end',
              }}>
              <Icons
                style={{ marginRight: 8, marginTop: 25 }}
                name="notifications"
                size={30}
                color="#fff"
                onPress={() => navigate('Notification')}
              />
            </View>
            <Text style={{ fontSize: 34, fontWeight: 'bold', marginLeft: 16, color: '#fff' }}>
              {I18n.t('Home.homeHeader')}
            </Text>
          </View>

          {Object.keys(data).length !== 0 ? (
            <Text style={{ marginLeft: 16, marginTop: 30 }}>บัญชีคุณ{data.meter_owner_name}</Text>
          ) : null}
          <Card containerStyle={{ borderRadius: 10 }}>
            <View style={{ flexDirection: 'row', width: '100%' }}>
              <Text style={{ marginBottom: 5, fontSize: 25, textAlign: 'left', width: '50%' }}>
                {`Sky's Home`}
              </Text>
              <Icons
                name="navigate-next"
                style={{ fontSize: 25, textAlign: 'right', width: '50%' }}
                onPress={() => navigate('CheckHistory')}
              />
            </View>
            <Text style={{ marginBottom: 5, fontSize: 25 }}>{I18n.t('Home.electricityBill')}</Text>
            <Text style={{ marginBottom: 5, fontSize: 30, fontWeight: 'bold', color: '#F26F21' }}>
              198.42
            </Text>
            <Text style={{ marginBottom: 5, fontSize: 20 }}>{I18n.t('Home.due')} 10/11/2562</Text>
            <Button
              buttonStyle={{
                marginLeft: 0,
                marginRight: 0,
                marginBottom: 0,
                borderRadius: 10,
                backgroundColor: '#F26F21',
              }}
              title={I18n.t('Home.pay')}
              titleStyle={{ color: '#fff' }}
              type="clear"
              onPress={() => navigate('SelectPayment')}
            />
          </Card>
          {this.showCard()}
          <View style={{ marginTop: 45, marginBottom: 100 }}>
            <Button
              title={I18n.t('Home.addElectricityMeter')}
              titleStyle={{ color: '#F26F21' }}
              type="clear"
              containerStyle={{ width: '100%' }}
              onPress={() => navigate('ScanQRcode')}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

HomeScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
