import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView, Text, View, StatusBar, Platform } from 'react-native';
import { ListItem, Button } from 'react-native-elements';
import Icons from 'react-native-vector-icons/MaterialIcons';
import I18n from '../src/i18n';

export default class AlretScreen extends Component {
  static navigationOptions = { headerShown: false };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{ height: '100%', backgroundColor: '#fff' }}>
        <StatusBar
          barStyle={Platform === 'ios' ? 'light-content' : 'dark-content'}
          backgroundColor="#fff"
        />
        <ScrollView ontentInsetAdjustmentBehavior="automatic">
          <View
            style={{
              alignItems: 'flex-end',
            }}>
            <Icons
              style={{ marginRight: 8, marginTop: 35 }}
              name="notifications"
              size={30}
              color="#F26F21"
              onPress={() => navigate('Notification')}
            />
          </View>
          <Text style={{ fontSize: 34, fontWeight: 'bold', marginLeft: 16, marginBottom: 8 }}>
            {I18n.t('Alret.alretHeader')}
          </Text>
          <ListItem
            title={I18n.t('Alret.specifyWhereToNotify')}
            bottomDivider
            titleStyle={{ color: '#fff', fontWeight: 'bold' }}
            containerStyle={{ backgroundColor: '#F26F21' }}
          />
          <ListItem
            leftIcon={{ name: 'home', type: 'entypo', color: '#F26F21' }}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            title={I18n.t('Alret.myHomeLocation')}
            bottomDivider
            onPress={() => navigate('SelectAlret')}
          />

          <ListItem
            leftIcon={{ name: 'user', type: 'entypo', color: '#F26F21' }}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            title={I18n.t('Alret.myCurrentPosition')}
            bottomDivider
            onPress={() => navigate('SelectAlretPublic')}
          />
          <View style={{ marginTop: 40 }}>
            <Button
              style={{ marginRight: 16, marginBottom: 0 }}
              title={I18n.t('Alret.checkHistoryAlret')}
              titleStyle={{ color: '#F26F21' }}
              type="clear"
              containerStyle={{ width: '100%' }}
              onPress={() => navigate('HistoryAlret')}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

AlretScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
