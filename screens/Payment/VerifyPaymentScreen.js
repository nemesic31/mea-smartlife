import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Platform } from 'react-native';
import { Input, Button, Divider } from 'react-native-elements';
import ToggleSwitch from 'toggle-switch-react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Hoshi } from 'react-native-textinput-effects';
import I18n from '../../src/i18n';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    flex: 2,
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  textHeader: {
    fontSize: 35,
    fontWeight: 'bold',
    marginBottom: 10,
    marginTop: 20,
    marginLeft: 20,
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 10,
    marginLeft: 20,
  },
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  dividerStyle: {
    marginTop: 15,
    marginBottom: 15,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#000',
  },
});

export default class VerifyPaymentScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('Payment.paymentHeader'),
    headerLeft: () => {
      if (Platform.OS === 'ios') {
        return (
          <TouchableOpacity onPress={() => navigation.navigate('PaymentCreditForm')}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Icon
                name="ios-arrow-back"
                style={{ fontSize: 25, color: '#E07333', marginLeft: 10 }}
              />
              <Text style={{ fontSize: 20, color: '#E07333', marginLeft: 10 }}>
                {I18n.t('HeaderBar.back')}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
      return null;
    },
  });

  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView ontentInsetAdjustmentBehavior="automatic" style={{ backgroundColor: '#fff' }}>
        <View style={styles.background}>
          <View>
            <Text
              style={{
                fontSize: 18,
                marginBottom: 16,
                marginLeft: 20,
                marginTop: 15,
                fontWeight: 'bold',
              }}>
              {`Sky's Home`}
            </Text>

            <Text style={{ fontSize: 14, marginBottom: 5, marginLeft: 20, color: '#4d4d4d' }}>
              {I18n.t('Payment.accountShowingContract')} 213456789
            </Text>
            <Text style={{ fontSize: 14, marginBottom: 14, marginLeft: 20, color: '#999999' }}>
              {I18n.t('Payment.addressPay')}
            </Text>
            <Text style={{ fontSize: 22, marginBottom: 5, marginLeft: 15 }}>
              {' '}
              {I18n.t('PaymentcreditForm.totalPayment')}{' '}
            </Text>
            <Text
              style={{
                fontSize: 34,
                marginBottom: 5,
                marginLeft: 20,
                fontWeight: 'bold',
                color: '#E07333',
              }}>
              204.42 {I18n.t('Payment.baht')}
            </Text>
            <Divider style={styles.dividerStyle} />
            <Text
              style={{
                fontSize: 22,
                marginBottom: 16,
                marginLeft: 20,
                fontWeight: 'bold',
                color: '#E07333',
              }}>
              {I18n.t('VerifyPayment.inputCreditCardHeader')}
            </Text>
            <View style={{ marginBottom: 20 }}>
              <Hoshi
                style={{ marginBottom: 12 }}
                label={I18n.t('VerifyPayment.inputCreditNum')}
                // this is used as active border color
                borderColor={'#F26F21'}
                // active border height
                borderHeight={3}
                inputPadding={16}
              />
              <Hoshi
                style={{ marginBottom: 12 }}
                label={I18n.t('VerifyPayment.inputCreditName')}
                // this is used as active border color
                borderColor={'#F26F21'}
                // active border height
                borderHeight={3}
                inputPadding={16}
              />
              <Hoshi
                style={{ marginBottom: 12 }}
                label={I18n.t('VerifyPayment.inputCreditDate')}
                // this is used as active border color
                borderColor={'#F26F21'}
                // active border height
                borderHeight={3}
                inputPadding={16}
              />
              <Hoshi
                style={{ marginBottom: 12 }}
                label={I18n.t('VerifyPayment.inputCreditCVC')}
                // this is used as active border color
                borderColor={'#F26F21'}
                // active border height
                borderHeight={3}
                inputPadding={16}
              />
            </View>
          </View>
          <View style={{ justifyContent: 'flex-end' }}>
            <Button
              title={I18n.t('VerifyPayment.confirmPayment')}
              type="clear"
              titleStyle={{ color: '#fff' }}
              buttonStyle={{
                backgroundColor: '#E07333',
                borderRadius: 5,
                marginLeft: 15,
                marginRight: 15,
              }}
              onPress={() => navigate('BillPayment')}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

VerifyPaymentScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
