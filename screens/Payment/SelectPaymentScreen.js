import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Platform,
  StatusBar,
  ScrollView,
} from 'react-native';
import { Card, Button, Divider } from 'react-native-elements';
import Icons from 'react-native-vector-icons/FontAwesome';
import Swiper from 'react-native-swiper';
import Icon from 'react-native-vector-icons/Ionicons';
import I18n from '../../src/i18n';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
    width: '100%',
  },
  dividerStyle: {
    marginTop: 20,
    marginBottom: 8,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#000',
  },
  wrapper: {
    alignItems: 'center',
  },
});

export default class SelectPaymentScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('Payment.paymentHeader'),
    headerRight: () => (
      <Button
        style={{ fontSize: 20, marginLeft: 10 }}
        titleStyle={{ color: '#E07333' }}
        title={I18n.t('SelectPayment.payAtCounterHeaderRight')}
        type="clear"
        containerStyle={{ marginRight: 8 }}
        onPress={() => navigation.navigate('QRcodemeter')}
      />
    ),
    headerLeft: () => {
      if (Platform.OS === 'ios') {
        return (
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Icon
                name="ios-arrow-back"
                style={{ fontSize: 25, color: '#E07333', marginLeft: 10 }}
              />
              <Text style={{ fontSize: 20, color: '#E07333', marginLeft: 10 }}>
                {I18n.t('HeaderBar.back')}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
      return null;
    },
  });

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <ScrollView ontentInsetAdjustmentBehavior="automatic">
          <View>
            <Text
              style={{
                fontSize: 18,
                marginBottom: 16,
                marginLeft: 20,
                marginTop: 15,
                fontWeight: 'bold',
              }}>
              {`Sky's Home`}
            </Text>

            <Text style={{ fontSize: 14, marginBottom: 5, marginLeft: 20, color: '#4d4d4d' }}>
              {I18n.t('Payment.accountShowingContract')} 213456789
            </Text>
            <Text style={{ fontSize: 14, marginBottom: 14, marginLeft: 20, color: '#999999' }}>
              {I18n.t('Payment.addressPay')}
            </Text>
            <Text style={{ fontSize: 22, marginBottom: 5, marginLeft: 20 }}>
              {I18n.t('SelectPayment.electricChargeAmount')}
            </Text>
            <Text
              style={{
                fontSize: 34,
                marginBottom: 5,
                marginLeft: 20,
                fontWeight: 'bold',
                color: '#E07333',
              }}>
              198.42 {I18n.t('Payment.baht')}
            </Text>
            <Text
              style={{
                fontSize: 18,
                marginBottom: 14,
                marginLeft: 20,
                fontWeight: 'bold',
                color: '#4d4d4d',
              }}>
              {I18n.t('Home.due')} 10/11/2562
            </Text>
            <Text
              style={{
                fontSize: 22,
                marginBottom: 16,
                marginLeft: 20,
                fontWeight: 'bold',
                color: '#E07333',
              }}>
              {I18n.t('SelectPayment.chooseThePayment')}
            </Text>
            <Text style={{ fontSize: 14, marginBottom: -10, marginLeft: 20, color: '#999999' }}>
              {I18n.t('SelectPayment.lastUsedPayment')}
            </Text>
            <TouchableOpacity onPress={() => navigate('PaymentCreditForm')}>
              <Card
                containerStyle={{
                  backgroundColor: '#efeff4',
                  borderRadius: 10,
                  justifyContent: 'center',
                  borderColor: '#efeff4',
                }}>
                <Text style={{ fontSize: 30, fontWeight: 'bold', color: '#E07333' }}>
                  {I18n.t('SelectPayment.fee')} 5 {I18n.t('Payment.baht')}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    flex: 2,
                    justifyContent: 'space-between',
                  }}>
                  <View>
                    <Text style={{ fontSize: 15, marginBottom: 5 }}>
                      {I18n.t('SelectPayment.payWithCreditCardsAllBanks')}
                    </Text>
                  </View>
                  <View s>
                    <Icons
                      name="cc-mastercard"
                      style={{ fontSize: 25, textAlign: 'right', color: '#E07333' }}
                    />
                  </View>
                </View>
              </Card>
            </TouchableOpacity>
          </View>
          <Divider style={styles.dividerStyle} />
          <View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                flex: 2,
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'bold',
                  marginLeft: 20,
                  marginTop: 10,
                }}>
                {I18n.t('SelectPayment.paymentMethod')}
              </Text>
              <Button
                title={I18n.t('SelectPayment.seeAll')}
                titleStyle={{ color: '#E07333' }}
                type="clear"
                containerStyle={{ alignItems: 'flex-end', marginRight: 10 }}
              />
            </View>

            <Swiper style={styles.wrapper} containerStyle={{ height: 175 }}>
              <TouchableOpacity onPress={() => navigate('PaymentCreditForm')}>
                <Card
                  containerStyle={{
                    backgroundColor: '#efeff4',
                    borderRadius: 10,
                    borderColor: '#efeff4',
                    justifyContent: 'center',
                  }}>
                  <Text style={{ fontSize: 30, color: '#E07333', fontWeight: 'bold' }}>
                    {I18n.t('SelectPayment.fee')} 6 {I18n.t('Payment.baht')}
                  </Text>
                  <Text style={{ fontSize: 15, marginBottom: 5 }}>
                    {I18n.t('SelectPayment.payWithCreditCardsAllBanks')}
                  </Text>
                </Card>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigate('PaymentCreditForm')}>
                <Card
                  containerStyle={{
                    backgroundColor: '#efeff4',
                    borderRadius: 10,
                    borderColor: '#efeff4',
                    justifyContent: 'center',
                  }}>
                  <Text style={{ fontSize: 30, color: '#E07333', fontWeight: 'bold' }}>
                    {I18n.t('SelectPayment.fee')} 10 {I18n.t('Payment.baht')}
                  </Text>
                  <Text style={{ fontSize: 15, marginBottom: 5 }}>
                    {I18n.t('SelectPayment.payWithInternetBankingAllBanks')}
                  </Text>
                </Card>
              </TouchableOpacity>
            </Swiper>
          </View>
        </ScrollView>
      </View>
    );
  }
}

SelectPaymentScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
