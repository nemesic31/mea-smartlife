import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Text, Platform } from 'react-native';
import { Button } from 'react-native-elements';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Icons from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/Ionicons';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    flex: 1,
  },
});

export default class NotificationScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitleAlign: 'left',
    headerShown: Platform !== 'ios',
    headerLeft: () => null,
    headerRight: () => <Button title="การตั้งค่า" type="clear" />,
  });

  render() {
    return (
      <View style={styles.background}>
        <Grid>
          {Platform === 'ios' && (
            <>
              <Row size={5}>
                <Col>
                  {/* <Text style={{ marginTop: 12, marginLeft: 16 }}>Back</Text> */}
                  <Icon
                    name="ios-arrow-back"
                    onPress={() => this.props.navigation.goBack()}
                    style={{ fontSize: 25, color: '#007AFF', marginLeft: 10 }}
                  />
                </Col>
                <Col style={{ alignItems: 'flex-end' }}>
                  <Button title="การตั้งค่า" type="clear" />
                </Col>
              </Row>
              <Row size={5}>
                <Text style={{ fontSize: 34, fontWeight: 'bold', marginLeft: 16 }}>Hi</Text>
              </Row>
            </>
          )}

          <Row size={Platform === 'ios' ? 90 : 100}>
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Icons
                style={{ marginRight: 8, marginTop: 8 }}
                name="notifications"
                size={100}
                color="#d1d1d1"
              />
              <Text style={{ fontSize: 22, fontWeight: 'bold' }}>ไม่มีการแจ้งเตือนในปัจุบัน</Text>
              <View>
                <Text style={{ fontSize: 14, paddingHorizontal: 35 }}>
                  แจ้งยอดค่าไฟฟ้าของคุณ, แจ้งเตือนให้ชำระค่าไฟฟ้าและแจ้งเตือนอื่นๆจะแสดงที่นี่
                </Text>
              </View>
            </View>
          </Row>
        </Grid>
      </View>
    );
  }
}

NotificationScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
