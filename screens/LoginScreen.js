import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import { Button } from 'react-native-elements';
import { Hoshi } from 'react-native-textinput-effects';
import Icon from 'react-native-vector-icons/Ionicons';
import I18n from '../src/i18n';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',
    alignItems: 'center',
    width: '100%',
    flex: 1,
  },
  button: { flex: 1, justifyContent: 'flex-end', marginBottom: 15 },
  textHeader: {
    fontSize: 34,
    fontWeight: 'bold',
    marginBottom: 40,
    marginTop: 15,
    marginLeft: 20,
    color: '#F26F21',
  },
  textHeader2: {
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 10,
  },
});

export default class LoginScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: '',
    headerShown: Platform.OS === 'ios',
    headerLeft: () => (
      <TouchableOpacity onPress={() => navigation.navigate('First')}>
        <View>
          <Icon name="ios-arrow-back" style={{ fontSize: 25, color: '#E07333', marginLeft: 10 }} />
        </View>
      </TouchableOpacity>
    ),
  });

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          enabled={Platform.OS === 'ios'}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 60 : 0}>
          <View style={styles.containner}>
            <View>
              <Text style={styles.textHeader}>{I18n.t('FirstScreen.login')}</Text>
              <View style={{ width: '95%', alignSelf: 'center' }}>
                <Hoshi
                  style={{ marginBottom: 10 }}
                  onChangeText={(v) => console.log(v)}
                  keyboardType={'numeric'}
                  label={I18n.t('Register.IDcardNumber')}
                  // this is used as active border color
                  borderColor={'#F26F21'}
                  // active border height
                  borderHeight={3}
                  inputPadding={16}
                  // this is used to set backgroundColor of label mask.
                  // please pass the backgroundColor of your TextInput container.
                  // backgroundColor={'#F9F7F6'}
                />
                <Hoshi
                  style={{ marginBottom: 16 }}
                  onChangeText={(v) => console.log(v)}
                  keyboardType={'numeric'}
                  label={I18n.t('Login.password')}
                  // this is used as active border color
                  borderColor={'#F26F21'}
                  // active border height
                  borderHeight={3}
                  inputPadding={16}
                  // this is used to set backgroundColor of label mask.
                  // please pass the backgroundColor of your TextInput container.
                  // backgroundColor={'#F9F7F6'}
                />
              </View>
            </View>
            <View style={styles.button}>
              <Button
                buttonStyle={{
                  marginRight: 10,
                  marginLeft: 10,
                  marginBottom: 10,
                  borderRadius: 5,
                  backgroundColor: '#F26F21',
                  width: 350,
                }}
                title={I18n.t('FirstScreen.login')}
                titleStyle={{ color: '#fff' }}
                type="solid"
                onPress={() => navigate('Home')}
              />
            </View>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

LoginScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
