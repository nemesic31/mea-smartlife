import { createStackNavigator } from 'react-navigation-stack';

import FirstScreen from '../screens/FirstScreen';
import ConditionScreen from '../screens/ConditionScreen';
import RegisterScreen from '../screens/RegisterScreen';
import VerifyScreen from '../screens/VerifyScreen';
import LoginScreen from '../screens/LoginScreen';

const RegisterStack = createStackNavigator({
  First: FirstScreen,
  Condition: ConditionScreen,
  Register: RegisterScreen,
  Verify: VerifyScreen,
  Login: LoginScreen,
});

export default RegisterStack;
