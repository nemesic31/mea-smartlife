import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet } from 'react-native';
import Icons from 'react-native-vector-icons/SimpleLineIcons';
import I18n from '../../../src/i18n';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  textHeader: {
    fontWeight: 'bold',
    fontSize: 45,
    marginBottom: 5,
    color: '#E07333',
  },
  textHeader2: {
    fontSize: 17,
  },
});

export default class CompleteProcessPublicScreen extends Component {
  static navigationOptions = { headerShown: false };

  render() {
    const { navigate } = this.props.navigation;
    setTimeout(() => {
      navigate('Alret');
    }, 1500);
    return (
      <View style={styles.background}>
        <Icons name="check" style={{ fontSize: 100, marginBottom: 20, color: '#4CD964' }} />
        <Text style={styles.textHeader}>{I18n.t('Complete.statusComplete')}</Text>
        <Text style={styles.textHeader2}>{I18n.t('Complete.statusCompleteDetail')}</Text>
      </View>
    );
  }
}

CompleteProcessPublicScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
