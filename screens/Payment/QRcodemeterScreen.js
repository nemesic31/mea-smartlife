import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AsyncStorage from '@react-native-community/async-storage';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { ButtonGroup } from 'react-native-elements';
import QRCode from 'react-native-qrcode-svg';
import Barcode from 'react-native-barcode-builder';

import Icon from 'react-native-vector-icons/Ionicons';
import I18n from '../../src/i18n';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',

    width: '100%',
  },

  textHeader2: {
    fontSize: 15,
    marginBottom: 10,
    marginLeft: 20,
    fontWeight: '100',
    marginRight: 20,
  },
});

export default class QRcodemeterScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('SelectPayment.payAtCounterHeaderRight'),
    headerLeft: () => (
      <TouchableOpacity onPress={() => navigation.navigate('SelectPayment')}>
        <View
          style={{
            flexDirection: 'row',
          }}>
          <Icon name="ios-arrow-back" style={{ fontSize: 25, color: '#E07333', marginLeft: 10 }} />
          <Text style={{ fontSize: 20, color: '#E07333', marginLeft: 10 }}>
            {I18n.t('HeaderBar.back')}
          </Text>
        </View>
      </TouchableOpacity>
    ),
  });

  constructor() {
    super();
    this.state = {
      selectedIndex: 0,
      getqrcode: '1234',
    };
  }

  getData = async () => {
    const qrcode = await AsyncStorage.getItem('@QR_Code:key');
    this.setState({ getqrcode: qrcode });
  };

  updateIndex = (selectedIndex) => {
    this.setState({ selectedIndex });
  };

  getQRcode() {
    const { getqrcode } = this.state;
    return <QRCode value={getqrcode} size={200} />;
  }

  Barcode() {
    return (
      <View>
        <Barcode value={this.state.getqrcode} format="CODE128" />
        <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>{this.state.getqrcode}</Text>
      </View>
    );
  }

  render() {
    const buttons = [I18n.t('QRcode&Barcode.qrcode'), I18n.t('QRcode&Barcode.barcode')];

    const { selectedIndex } = this.state;
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.background}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'flex-end',
            height: '80%',
            width: '100%',
          }}>
          <View
            style={{
              width: '100%',
              height: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            {this.state.selectedIndex === 0 ? this.getQRcode() : this.Barcode()}
          </View>

          <Text style={styles.textHeader2}>{I18n.t('QRcode&Barcode.detailQR&Barcode')}</Text>
          <ButtonGroup
            onPress={this.updateIndex}
            selectedIndex={selectedIndex}
            buttons={buttons}
            containerStyle={{ height: 50 }}
            selectedButtonStyle={{ backgroundColor: '#E07333' }}
          />
        </View>
      </View>
    );
  }
}

QRcodemeterScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
