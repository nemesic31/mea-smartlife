import React, { Component } from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  Platform,
  KeyboardAvoidingView,
} from 'react-native';
import { Button } from 'react-native-elements';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';
import { Formik } from 'formik';
import { Hoshi } from 'react-native-textinput-effects';
import I18n from '../../src/i18n';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',

    width: '100%',
  },
  textHeader: {
    fontSize: 35,
    fontWeight: 'bold',
    marginBottom: 10,
    marginTop: 20,
    marginLeft: 20,
    color: '#E07333',
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 20,
    marginLeft: 20,
    marginRight: 10,
  },
});

export default class FillmeterScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('Add-meter.addMeterHeader'),
    headerTintColor: '#fff',
    headerStyle: {
      backgroundColor: '#E07333',
    },

    headerLeft: () => {
      if (Platform.OS === 'ios') {
        return (
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Icon name="ios-arrow-back" style={{ fontSize: 25, color: '#fff', marginLeft: 10 }} />
              <Text style={{ fontSize: 20, color: '#fff', marginLeft: 10 }}>
                {I18n.t('HeaderBar.back')}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
      return null;
    },
  });

  render() {
    const { navigate } = this.props.navigation;
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        enabled={Platform.OS === 'ios'}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 60 : 0}>
        <View style={styles.background}>
          <Text style={styles.textHeader}>{I18n.t('Fillmeter.fillMeterHeader')} </Text>
          <Text style={styles.textHeader2}>{I18n.t('Fillmeter.fillMeterHeader2')}</Text>
          <Formik
            initialValues={{ account_no: '213465879', meter_no: '213465879' }}
            onSubmit={(values) =>
              navigate('Namemeter', { account_no: values.account_no, meter_no: values.meter_no })
            }>
            {({ handleChange, handleBlur, handleSubmit, values }) => (
              <View>
                <Hoshi
                  style={{ marginBottom: 10 }}
                  label={I18n.t('Fillmeter.inputMeter')}
                  // this is used as active border color
                  borderColor={'#F26F21'}
                  // active border height
                  borderHeight={3}
                  inputPadding={16}
                  keyboardType="numeric"
                  onChangeText={handleChange('account_no')}
                  onBlur={handleBlur('account_no')}
                  value={values.account_no}
                  // this is used to set backgroundColor of label mask.
                  // please pass the backgroundColor of your TextInput container.
                  // backgroundColor={'#F9F7F6'}
                />
                <Hoshi
                  style={{ marginBottom: 10 }}
                  label={I18n.t('Fillmeter.inputMeter2')}
                  // this is used as active border color
                  borderColor={'#F26F21'}
                  // active border height
                  borderHeight={3}
                  inputPadding={16}
                  keyboardType="numeric"
                  onChangeText={handleChange('account_no')}
                  onBlur={handleBlur('account_no')}
                  value={values.account_no}
                  // this is used to set backgroundColor of label mask.
                  // please pass the backgroundColor of your TextInput container.
                  // backgroundColor={'#F9F7F6'}
                />
                {/* <Button onPress={handleSubmit} title="Submit" /> */}
                <View style={{ justifyContent: 'flex-end', height: '30%' }}>
                  <Button
                    title={I18n.t('Register.next')}
                    titleStyle={{ color: '#A3A3A3' }}
                    type="clear"
                    buttonStyle={{
                      backgroundColor: '#EFEFF4',
                      borderRadius: 10,
                      marginLeft: 10,
                      marginRight: 10,
                      marginBottom: 5,
                    }}
                    // onPress={() => navigate('Namemeter')}
                    onPress={handleSubmit}
                  />
                  <Button
                    title={I18n.t('Fillmeter.addMeterByQRcode')}
                    titleStyle={{ color: '#E07333' }}
                    type="clear"
                    buttonStyle={{
                      marginLeft: 10,
                      marginRight: 10,
                    }}
                    onPress={() => navigate('ScanQRcode')}
                  />
                </View>
              </View>
            )}
          </Formik>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

FillmeterScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
