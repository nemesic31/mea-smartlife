import React, { Component } from 'react';
import { TouchableOpacity, Platform, Text, View, StyleSheet, ScrollView } from 'react-native';
import { Button } from 'react-native-elements';
import ToggleSwitch from 'toggle-switch-react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Formik } from 'formik';
import axios from 'axios';
import PropTypes from 'prop-types';
import { Hoshi } from 'react-native-textinput-effects';
import I18n from '../../src/i18n';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',

    width: '100%',
  },
  textHeader: {
    fontSize: 18,
    fontWeight: 'bold',
    marginVertical: 10,
    marginLeft: 20,
  },
  textHeader3: {
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 20,
    marginLeft: 20,
    color: '#E07333',
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 5,
    marginLeft: 20,
  },
});

export default class NamemeterScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('Add-meter.addMeterHeader'),
    headerTintColor: '#fff',
    headerStyle: {
      backgroundColor: '#E07333',
    },
    headerLeft: () => {
      if (Platform.OS === 'ios') {
        return (
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Icon name="ios-arrow-back" style={{ fontSize: 25, color: '#fff', marginLeft: 10 }} />
              <Text style={{ fontSize: 20, color: '#fff', marginLeft: 10 }}>
                {I18n.t('HeaderBar.back')}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
      return null;
    },
  });

  constructor() {
    super();
    this.state = {
      toggle: false,
      qrcode: '',
      meterData: {},
    };
  }

  componentDidMount() {
    // this.getData();
    this.getMeterNo();
  }

  check = (event) => {
    console.log(event);
  };

  // getData = async () => {
  //   const qrcode = await AsyncStorage.getItem('@QR_Code:key');
  //   console.log({ qrcode });
  //   await this.setState({ qrcode });
  // };

  getMeterNo = () => {
    const { navigation } = this.props;
    const account_no = JSON.stringify(navigation.getParam('account_no', '213465879')).slice(1, -1);
    const meter_no = JSON.stringify(navigation.getParam('meter_no', '10267112')).slice(1, -1);
    axios
      .post('https://yena.entercorp.net/api/v0.1.0/smart-meters/request-data', {
        meter_no,
        contract_account_no: account_no,
      })
      .then((response) => this.setState({ meterData: response.data }))
      .catch((err) => console.log(err));
  };

  render() {
    const { navigate } = this.props.navigation;
    const { qrcode, meterData } = this.state;
    return (
      <View style={styles.background}>
        <ScrollView ontentInsetAdjustmentBehavior="automatic" style={{ height: '100%' }}>
          <View style={{ marginBottom: '50%' }}>
            <Text style={styles.textHeader}>{I18n.t('Namemeter.meterDataHeader')} </Text>
            <Hoshi
              style={{ marginBottom: 10 }}
              label={I18n.t('Fillmeter.inputMeter')}
              // this is used as active border color
              borderColor={'#F26F21'}
              // active border height
              borderHeight={3}
              inputPadding={16}
              value={meterData.contract_account_no}
            />
            <Hoshi
              style={{ marginBottom: 10 }}
              label={I18n.t('Fillmeter.inputMeter2')}
              // this is used as active border color
              borderColor={'#F26F21'}
              // active border height
              borderHeight={3}
              inputPadding={16}
              editable={false}
              value={meterData.meter_no}
            />
            <Hoshi
              style={{ marginBottom: 10 }}
              label={I18n.t('Namemeter.ownerMeter')}
              // this is used as active border color
              borderColor={'#F26F21'}
              // active border height
              borderHeight={3}
              inputPadding={16}
              editable={false}
              value={meterData.meter_owner_name}
            />
            <Hoshi
              style={{ marginBottom: 10 }}
              label={I18n.t('Namemeter.address')}
              // this is used as active border color
              borderColor={'#F26F21'}
              // active border height
              borderHeight={3}
              inputPadding={16}
              editable={false}
              value={meterData.meter_address}
            />

            <Text style={styles.textHeader3}>{I18n.t('Namemeter.nameMeterHeader')}</Text>
            <Formik
              initialValues={{ meter_name: '' }}
              onSubmit={(values) => {
                axios
                  .post('https://yena.entercorp.net/api/v0.1.0/smart-meters', {
                    user_id: '826fbd48-3606-4670-9891-da3ca56d6f2b',
                    meter_no: meterData.meter_no,
                    meter_name: values.meter_name,
                    contract_account_no: meterData.contract_account_no,
                    required_life_support: this.state.toggle,
                  })
                  .then((response) => {
                    const data = {
                      meter_name: values.meter_name,
                      contract_account_no: meterData.contract_account_no,
                      meter_address: meterData.meter_address,
                      meter_owner_name: meterData.meter_owner_name,
                      bill: response.data,
                    };
                    navigate('Home', { data });
                  })
                  .catch((err) => console.log(err));
              }}>
              {({ handleChange, handleBlur, handleSubmit, values }) => (
                <>
                  <Hoshi
                    style={{ marginBottom: 10 }}
                    label={I18n.t('Namemeter.nameMeterInput')}
                    // this is used as active border color
                    borderColor={'#F26F21'}
                    // active border height
                    borderHeight={3}
                    inputPadding={16}
                    onChangeText={handleChange('meter_name')}
                    onBlur={handleBlur('meter_name')}
                    value={values.meter_name}
                  />
                  <ToggleSwitch
                    isOn={this.state.toggle}
                    onColor="#E07333"
                    offColor="gray"
                    label={I18n.t('Namemeter.patientToggle')}
                    labelStyle={{ marginLeft: 10, fontSize: 17, marginRight: 25 }}
                    size="large"
                    onToggle={() =>
                      this.state.toggle
                        ? this.setState({ toggle: false })
                        : this.setState({ toggle: true })
                    }
                  />
                  <View style={{ backgroundColor: '#efeff4', marginTop: 10 }}>
                    <Text
                      style={{
                        marginLeft: 20,
                        marginTop: 10,
                        marginBottom: 10,
                        color: '#8a8a8f',
                        fontWeight: '500',
                        textAlign: 'left',
                      }}>
                      {I18n.t('Namemeter.patientDetail')}
                    </Text>
                  </View>
                  <Button
                    title={I18n.t('Namemeter.confirm')}
                    titleStyle={{ color: '#fff' }}
                    type="clear"
                    style={{ marginTop: 20 }}
                    onPress={handleSubmit}
                    buttonStyle={{
                      backgroundColor: '#E07333',
                      marginRight: 15,
                      marginLeft: 15,
                      borderRadius: 10,
                    }}
                  />
                  <Button
                    title={I18n.t('Namemeter.namedLater')}
                    titleStyle={{ color: '#E07333' }}
                    type="clear"
                    style={{ marginTop: 20 }}
                    onPress={handleSubmit}
                  />
                </>
              )}
            </Formik>
          </View>
        </ScrollView>
      </View>
    );
  }
}

NamemeterScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
