import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView, Text, View, StatusBar } from 'react-native';
import { ListItem, Divider } from 'react-native-elements';
import Icons from 'react-native-vector-icons/MaterialIcons';
import I18n from '../src/i18n';

export default class SettingScreen extends Component {
  static navigationOptions = { headerShown: false };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{ height: '100%', backgroundColor: '#fff' }}>
        <StatusBar barStyle="dark-content" backgroundColor="#F26F21" />
        <ScrollView ontentInsetAdjustmentBehavior="automatic">
          <View style={{ backgroundColor: '#F26F21' }}>
            <View
              style={{
                alignItems: 'flex-end',
              }}>
              <Icons
                style={{ marginRight: 8, marginTop: 35 }}
                name="notifications"
                size={30}
                color="#fff"
                onPress={() => navigate('Notification')}
              />
            </View>
            <Text
              style={{
                fontSize: 34,
                fontWeight: 'bold',
                marginLeft: 16,
                marginBottom: 8,
                color: '#fff',
              }}>
              {I18n.t('Setting.settingHeader')}
            </Text>
          </View>

          <Divider style={{ backgroundColor: '#d0d0d0' }} />
          <ListItem
            bottomDivider
            title="ท้องฟ้า แจ่มใส"
            subtitle={`id: 81990253xxxx\n${I18n.t('Setting.tel')}: 0997891234`}
            subtitleStyle={{ fontSize: 16 }}
            style={{
              paddingBottom: 10,
              paddingTop: 10,
              marginTop: 10,
              marginBottom: 10,
            }}
            titleStyle={{ fontSize: 30, color: '#F26F21' }}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            leftIcon={{
              name: 'user',
              type: 'entypo',
              color: '#F26F21',
              size: 50,
              marginRight: 10,
              marginLeft: 10,
            }}
          />

          <ListItem
            bottomDivider
            title={I18n.t('Setting.paymentLocation')}
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            leftIcon={{ name: 'store', type: 'font-awesome5', color: '#F26F21' }}
          />
          <ListItem
            bottomDivider
            title={I18n.t('Setting.powerOutageAnnouncement')}
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            leftIcon={{ name: 'megaphone', type: 'foundation', color: '#F26F21' }}
          />
          <ListItem
            bottomDivider
            title={I18n.t('Setting.requestToUseElectricity')}
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            leftIcon={{ name: 'flash', type: 'font-awesome', color: '#F26F21' }}
          />

          <ListItem
            bottomDivider
            title={I18n.t('Setting.calculateElectricityBill')}
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            leftIcon={{ name: 'ios-calculator', type: 'ionicon', color: '#F26F21' }}
          />
          <ListItem
            bottomDivider
            title={I18n.t('Setting.emergencyNumber')}
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            leftIcon={{ name: 'ios-call', type: 'ionicon', color: '#F26F21' }}
          />
          <ListItem
            bottomDivider
            title={I18n.t('Setting.systemSettings')}
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            leftIcon={{ name: 'ios-settings', type: 'ionicon', color: '#F26F21' }}
          />
          <ListItem
            bottomDivider
            title={I18n.t('Setting.instructionManual')}
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            leftIcon={{ name: 'ios-book', type: 'ionicon', color: '#F26F21' }}
          />
          <ListItem
            bottomDivider
            title={I18n.t('Setting.about')}
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            leftIcon={{ name: 'ios-information-circle', type: 'ionicon', color: '#F26F21' }}
          />
          <ListItem
            bottomDivider
            title={I18n.t('Setting.logout')}
            containerStyle={{ margin: 0, marginBottom: 2 }}
            onPress={() => navigate('First')}
            rightIcon={{ name: 'navigate-next', color: '#F26F21' }}
            leftIcon={{ name: 'log-out', type: 'entypo', color: '#F26F21' }}
          />
          <View style={{ marginBottom: 20 }}></View>
        </ScrollView>
      </View>
    );
  }
}

SettingScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
