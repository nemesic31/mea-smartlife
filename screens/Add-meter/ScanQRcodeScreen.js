import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  Platform,
  TouchableOpacity,
  PermissionsAndroid,
  StatusBar,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Row, Grid } from 'react-native-easy-grid';
import { Button } from 'react-native-elements';
import { CameraKitCameraScreen } from 'react-native-camera-kit';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';
import I18n from '../../src/i18n';

const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor: '#fff',
    flex: 1,

    paddingTop: Platform.OS === 'ios' ? 20 : 0,
  },
  QR_text: {
    color: '#000',
    fontSize: 19,
    padding: 8,
    marginTop: 12,
  },
  button: {
    backgroundColor: '#2979FF',
    alignItems: 'center',
    padding: 12,
    width: 300,
    marginTop: 14,
  },
  background: {
    backgroundColor: '#fff',
    height: '100%',

    width: '100%',
  },
  textHeader: {
    fontSize: 35,
    fontWeight: 'bold',
    marginBottom: 10,
    marginTop: 2,
    marginLeft: 20,
    color: '#E07333',
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 10,
    marginLeft: 20,
  },
});

export default class ScanQRcodeScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('Add-meter.addMeterHeader'),
    headerTintColor: '#fff',
    headerStyle: {
      backgroundColor: '#E07333',
    },

    headerLeft: () => {
      if (Platform.OS === 'ios') {
        return (
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Icon name="ios-arrow-back" style={{ fontSize: 25, color: '#fff', marginLeft: 10 }} />
              <Text style={{ fontSize: 20, color: '#fff', marginLeft: 10 }}>
                {I18n.t('HeaderBar.back')}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
      return null;
    },
  });

  constructor() {
    super();
    this.state = {
      QR_Code_Value: '',
      Start_Scanner: false,
    };
  }

  componentDidMount() {
    this.open_QR_Code_Scanner();
  }

  openLink_in_browser = () => {
    // Linking.openURL(this.state.QR_Code_Value);
  };

  onQR_Code_Scan_Done = async (QR_Code) => {
    await AsyncStorage.setItem('@QR_Code:key', QR_Code);
    this.props.navigation.navigate('Namemeter', { QR_Code });

    // this.setState({ Start_Scanner: false });
  };

  open_QR_Code_Scanner = async () => {
    const that = this;
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA, {
          title: 'Camera App Permission',
          message: 'Camera App needs access to your camera ',
        });
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          that.setState({ QR_Code_Value: '' });
          // that.setState({ Start_Scanner: true });
        } else {
          alert('CAMERA permission denied');
        }
      } catch (err) {
        alert('Camera permission err', err);
        console.warn(err);
      }
    }
  };

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.MainContainer}>
        <StatusBar
          backgroundColor={Platform.OS === 'ios' ? 'white' : '#F26F21'}
          barStyle="dark-content"
        />
        <Grid>
          <Row size={15}>
            <View>
              <Text style={styles.textHeader}>{I18n.t('ScanQRcode.scanQRcodeHeader')}</Text>
              <Text style={styles.textHeader2}>{I18n.t('ScanQRcode.scanQRcodeHeader2')}</Text>
            </View>
          </Row>
          <Row size={75}>
            <View
              style={
                Platform.OS === 'ios' && {
                  height: '60%',
                  justifyContent: 'center',
                  width: '100%',
                }
              }>
              <CameraKitCameraScreen
                style={{ height: '100%' }}
                showFrame={true}
                scanBarcode={true}
                laserColor={'#FF3D00'}
                frameColor={'#00C853'}
                colorForScannerFrame={'black'}
                onReadCode={(event) => this.onQR_Code_Scan_Done(event.nativeEvent.codeStringValue)}
              />
            </View>
          </Row>
          <Row size={10} style={{ justifyContent: 'center' }}>
            <View>
              <View style={Platform.OS !== 'ios' && { width: 400, justifyContents: 'center' }}>
                <Button
                  title={I18n.t('ScanQRcode.fillMeterButton')}
                  titleStyle={{ color: '#E07333' }}
                  type={Platform.OS === 'ios' ? 'clear' : 'solid'}
                  buttonStyle={Platform.OS !== 'ios' && { backgroundColor: '#dfdfdf' }}
                  onPress={() => navigate('Fillmeter')}
                />
              </View>
            </View>
          </Row>
        </Grid>
      </View>
    );
  }
}

ScanQRcodeScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
