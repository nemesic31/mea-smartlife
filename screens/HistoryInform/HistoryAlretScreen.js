import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Platform,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  StatusBar,
} from 'react-native';
import { Card } from 'react-native-elements';
import Icons from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/Ionicons';
import I18n from '../../src/i18n';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',

    width: '100%',
  },
  textHeader: {
    fontSize: 13,
    marginBottom: 5,
    width: '50%',
    color: '#E07333',
  },
  textHeader4: {
    fontSize: 13,
    marginBottom: 5,
    width: '50%',
    textAlign: 'right',
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 15,
    marginTop: 25,
    width: '50%',
  },
  textHeader3: {
    fontSize: 15,
    marginBottom: 15,
    marginTop: 25,
    width: '50%',
    textAlign: 'right',
    color: '#E07333',
  },
});

export default class HistoryAlertScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('HistoryAlret.notificationHistory'),
    headerTintColor: '#ffffff',
    headerStyle: {
      backgroundColor: '#E07333',
    },
    headerLeft: () => {
      if (Platform.OS === 'ios') {
        return (
          <TouchableOpacity onPress={() => navigation.navigate('Alret')}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Icon name="ios-arrow-back" style={{ fontSize: 25, color: '#fff', marginLeft: 10 }} />
              <Text style={{ fontSize: 20, color: '#fff', marginLeft: 10 }}>
                {I18n.t('HeaderBar.back')}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
      return null;
    },
  });

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <StatusBar
          backgroundColor={Platform.OS === 'ios' ? 'white' : '#F26F21'}
          barStyle="dark-content"
        />
        <ScrollView ontentInsetAdjustmentBehavior="automatic">
          <TouchableOpacity onPress={() => navigate('DetailHistoryAlret')}>
            <Card containerStyle={{ borderColor: '#E07333', borderRadius: 10 }}>
              <View style={{ flexDirection: 'row', width: '100%' }}>
                <Text style={styles.textHeader}>{I18n.t('HistoryAlret.notifyAlretHeader')}</Text>
                <Text style={styles.textHeader4}>25/10/62</Text>
              </View>

              <View style={{ flexDirection: 'row', width: '100%' }}>
                <Text style={styles.textHeader2}>{I18n.t('HistoryAlret.statusAlret')} </Text>

                <Text style={styles.textHeader3}>
                  {I18n.t('HistoryAlret.status')}
                  <Icons name="navigate-next" style={{ fontSize: 15, color: '#E07333' }} />
                </Text>
              </View>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigate('DetailHistoryAlret')}>
            <Card containerStyle={{ borderColor: '#E07333', borderRadius: 10 }}>
              <View style={{ flexDirection: 'row', width: '100%' }}>
                <Text style={styles.textHeader}>
                  {I18n.t('HistoryAlret.notifyAlretHeaderHome')}
                </Text>
                <Text style={styles.textHeader4}>25/10/62</Text>
              </View>

              <View style={{ flexDirection: 'row', width: '100%' }}>
                <Text style={styles.textHeader2}>{I18n.t('HistoryAlret.statusAlret')} </Text>

                <Text style={styles.textHeader3}>
                  {I18n.t('HistoryAlret.status')}
                  <Icons name="navigate-next" style={{ fontSize: 15, color: '#E07333' }} />
                </Text>
              </View>
            </Card>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

HistoryAlertScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
