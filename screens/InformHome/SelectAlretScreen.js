import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Platform,
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  StatusBar,
  ScrollView,
} from 'react-native';
import { ListItem } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import I18n from '../../src/i18n';

const styles = StyleSheet.create({
  background: {
    height: '100%',
  },
  textHeader: {
    fontSize: 25,
    fontWeight: 'bold',
    marginBottom: 15,
    marginTop: 20,
    marginLeft: 10,
    color: '#E07333',
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 40,
    marginLeft: 15,
    color: '#7D7E82',
  },
  textHeader3: {
    fontSize: 15,
    marginLeft: 15,
    marginBottom: 10,
    marginTop: 20,
    color: '#7D7E82',
  },
});

export default class SelectAlretScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('InformHome.informHeader'),
    headerLeft: () => {
      if (Platform.OS === 'ios') {
        return (
          <TouchableOpacity onPress={() => navigation.navigate('Alret')}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Icon
                name="ios-arrow-back"
                style={{ fontSize: 25, color: '#E07333', marginLeft: 10 }}
              />
              <Text style={{ fontSize: 20, color: '#E07333', marginLeft: 10 }}>
                {I18n.t('HeaderBar.back')}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
      return null;
    },
  });

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <ScrollView ontentInsetAdjustmentBehavior="automatic">
          <Text style={styles.textHeader}>{I18n.t('SelectAlret.informHomeHeader')}</Text>
          <Text style={styles.textHeader2}>{I18n.t('InformHome.addressInform')}</Text>
          <Text style={styles.textHeader3}>{I18n.t('InformHome.selectHeader')}</Text>
          <ListItem
            title={I18n.t('SelectAlret.listitemHomeSelectHeader')}
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#E07333' }}
            titleStyle={{ color: '#E07333' }}
            onPress={() => navigate('Map')}
          />
          <Text style={styles.textHeader3}>{I18n.t('InformHome.selectHeader2')}</Text>
          <ListItem
            title={I18n.t('SelectAlret.listitemHomeSelectHeader2')}
            titleStyle={{ color: '#E07333' }}
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#E07333' }}
            onPress={() => navigate('Map')}
          />
          <ListItem
            title={I18n.t('SelectAlret.listitem2HomeSelectHeader2')}
            titleStyle={{ color: '#E07333' }}
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#E07333' }}
            onPress={() => navigate('Map')}
          />
          <ListItem
            title={I18n.t('SelectAlret.listitem3HomeSelectHeader2')}
            titleStyle={{ color: '#E07333' }}
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#E07333' }}
            onPress={() => navigate('Map')}
          />
          <ListItem
            title={I18n.t('SelectAlret.listitem4HomeSelectHeader2')}
            titleStyle={{ color: '#E07333' }}
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#E07333' }}
            onPress={() => navigate('Map')}
          />
          <Text style={styles.textHeader3}>{I18n.t('InformHome.selectHeader3')}</Text>
          <ListItem
            title={I18n.t('SelectAlret.listitemHomeSelectHeader3')}
            titleStyle={{ color: '#E07333' }}
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#E07333' }}
            onPress={() => navigate('Map')}
          />
          <ListItem
            title={I18n.t('SelectAlret.listitem2HomeSelectHeader3')}
            titleStyle={{ color: '#E07333' }}
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#E07333' }}
            onPress={() => navigate('Map')}
          />
          <View style={{ marginBottom: 20 }}></View>
        </ScrollView>
      </View>
    );
  }
}

SelectAlretScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};
